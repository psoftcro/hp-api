

function napunieuro(){
	if ($('#izracuneur').length>0){
		rezeuro=$('#rezultat').html().replace(',','.')*1/eur;
		$('#izracuneur').html("(" +rezeuro.toFixed(2) + " EUR)");
	}
}

function prikazi(kaj){
	if (document.getElementById(kaj).style.visibility=='hidden'){
		document.getElementById(kaj).style.visibility='visible';
	}else{
		document.getElementById(kaj).style.visibility='hidden';
	}
}
function Mid(str, start, len){
    // Make sure start and len are within proper bounds
    if (start < 0 || len < 0) return "";

    var iEnd, iLen = String(str).length;
    if (start + len > iLen)
            iEnd = iLen;
    else
            iEnd = start + len;

    return String(str).substring(start,iEnd);
}

function Len(str){  
	return String(str).length;  
}

function InStr(strSearch, charSearchFor)
{
	for (i=0; i < Len(strSearch); i++)
	{
	    if (charSearchFor == Mid(strSearch, i, 1))
	    {
			return i;
	    }
	}
	return -1;
}
function izracunajpismo(x){
	tezinapismou = parseFloat(x.tezinapismou.value);
	
	var temp = tezinapismou;

	
	
	
	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0"; 
	if (a<0)temp=temp+'.00';
	temp=temp.toString();
	$('#rezultat')[0].innerHTML= temp.replace('.',',');
	napunieuro();
}

function izracunajppismo(x){
	tezinapismou = parseFloat(x.tezinapismoup.value);
	
	
	var temp = tezinapismou;


	
	
	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0"; 
	if (a<0)temp=temp+'.00';
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}

function izracunajdopis(x){
	tezinapismou = parseFloat(x.tezinadopisu.value);
	

	
	var temp = tezinapismou;

	

	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0";
	if (a<0)temp=temp+'.00';
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}

function izracunajtiskanica(x){
	tezinapismou = parseFloat(x.tezinatisaku.value);
	dop1 = parseFloat(x.dop1tu.value);
	var temp = tezinapismou;
	if (x.dop1tu.checked) temp=temp+dop1;


	
	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0";
	if (a<0)temp=temp+'.00';
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}


function izracunajprepou(x){
	tezinapismou = parseFloat(x.tezinaprep.value);
	
	dop4 = parseFloat(x.dop4pp.value);
	dop5 = parseFloat(x.dop5pp.value);
	dop5b = parseFloat(x.dop5ppb.value);
	dop7 = parseFloat(x.dop7pp.value);
	
	
	if (tezinapismou==8.7) dop4=1.5;
	
	
	var temp = tezinapismou;
	
	if (x.dop4pp.checked) temp=temp+dop4;
	if (x.dop5pp.checked) temp=temp+dop5;
	if (x.dop5ppb.checked) temp=temp+dop5b;
	if (x.dop7pp.checked) temp=temp+dop7;
	
	
	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0"; 
	if (a<0)temp=temp+'.00';
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}

function izracunajpotvrduru(x){
	tezinapismou = parseFloat(x.tezinaprepa.value);

	dop5 = parseFloat(x.dop5ppa.value);
	
	var temp = tezinapismou;
	//temp=temp+7.4+1.9;
	if (x.dop5ppa.checked) temp=temp+dop5;
	
	
	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0"; 
	if (a<0)temp=temp+'.00';
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}

function izracunajvrijedu(x){
	tezinapismou = parseFloat(x.tezinavrijedu.value);
	
	
	dop4 = parseFloat(x.dop4vp.value);
	dop5 = parseFloat(x.dop5vp.value);
	dop5b = parseFloat(x.dop5vpb.value);
	dop7 = parseFloat(x.dop7vp.value);
	
	vrijednostpismou = x.vrijednostvrijedu.value;
	vrijednostpismou = vrijednostpismou.replace(",",".");
	vrijednostpismou = parseFloat(vrijednostpismou);
	
	if (isNaN(vrijednostpismou)){
		swal('Upišite vrijednost pošiljke')
		return false;
	}
	
	if (vrijednostpismou>100000){
		swal('Maksimalna vrijednost pošiljke je 100000,00 kn');
		return false;
	}
	
	if (vrijednostpismou<7.2){
		swal('Minimalna vrijednost pošiljke je 7,20 kn');
		return false;
	}
	
	var temp = tezinapismou;
	
	
	if (x.dop4vp.checked) temp=temp+dop4;
	if (x.dop5vp.checked) temp=temp+dop5;
	if (x.dop5vpb.checked) temp=temp+dop5b;
	if (x.dop7vp.checked) temp=temp+dop7;
	
	var temp1;
	if (vrijednostpismou<=200) temp1=7.2;
	if (vrijednostpismou>200 && vrijednostpismou<=1000) temp1=15;
	if (vrijednostpismou>1000 && vrijednostpismou<=10000) temp1=60;
	if (vrijednostpismou>10000 && vrijednostpismou<=50000) temp1=120;
	if (vrijednostpismou>50000) temp1=300;
	
	temp=temp+temp1;
	
	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0"; 
	if (a<0)temp=temp+'.00';
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}

function izracunajpaket(x){
	tezinapaketu = parseFloat(x.tezinapaketu.value);
	predanogdje=x.izborpaketa.value
	dop2 = parseFloat(x.dop2b.value);
	dop3 = parseFloat(x.dop3b.value);
	dop4 = parseFloat(x.dop4b.value);
	dop5 = parseFloat(x.dop5b.value);
	dop6 = parseFloat(x.dop6b.value);
	dop7 = parseFloat(x.dop7b.value);


	var minvrijednost=3;
	
	vrijednostpaketu = x.vrijednostpaketu.value;
	vrijednostpaketu = vrijednostpaketu.replace(",",".");
	vrijednostpaketu = parseFloat(vrijednostpaketu);
	
	if (isNaN(vrijednostpaketu)){
		swal('Upišite vrijednost pošiljke')
		return false;
	}
	
	if (vrijednostpaketu>100000){
		swal('Maksimalna vrijednost paketa je 100000,00 kn');
		return false;
	}
	
	
	var temp = tezinapaketu;
	var postotkauvecanje;
	
	if (tezinapaketu==1){ 
		if (predanogdje==1){
			temp=23;
		}else{
			temp=25;
		}
		postotkauvecanje=1;
	}else if (tezinapaketu==2){ 
		if (predanogdje==1){
			temp=25;
		}else{
			temp=30;
		}
		postotkauvecanje=1;
	}else if (tezinapaketu==3){ 
		if (predanogdje==1){
			temp=28;
		}else{
			temp=33;
		}
		postotkauvecanje=1;
	}else if (tezinapaketu==4){ 
		if (predanogdje==1){
			temp=35;
		}else{
			temp=40;
		}
		postotkauvecanje=1.25;
	}else if (tezinapaketu==5){ 
		if (predanogdje==1){
			temp=38;
		}else{
			temp=43;
		}
		postotkauvecanje=1.25;
	}
	//if (tezinapaketu>3) minvrijednost=3.66; //ako je teze od 10kg
	
	 
	
	if (x.dop2b.checked) temp=temp+(dop2*postotkauvecanje);
	if (x.dop3b.checked) temp=temp+(dop3*postotkauvecanje);
	if (x.dop4b.checked) temp=temp+dop4;
	if (x.dop5b.checked) temp=temp+(dop5*postotkauvecanje);
	if (x.dop6b.checked) temp=temp+(dop6*postotkauvecanje);
	if (x.dop7b.checked) temp=temp+dop7;
	
	var temp1 = (vrijednostpaketu*minvrijednost/100);
	if (temp1<3) temp1=3;
	if (temp1>220) temp1=220;
	if (temp1>0 && vrijednostpaketu>100) temp=temp+temp1;
	
	//var temp2 = (temp*50/100);
	
	
	
	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0";
	if (a<0)temp=temp+'.00';
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}

function provjeravrijednosti(){
	var arr= new Array() 
	var temp = document.getElementById("vrijedodm").value;
	arr=temp.split("|");
	
	if (arr[3]!="DA"){

		if (lng=="hr") {
			swal('Prioritetna vrijednosna pošiljka ne može se poslati u državu odredišta');
		}else{
			swal('Priority insured item not available for destination country');
		}
		
		return false;
	}else{
		return true;
	}

	
}


function provjeravrijednostipaket(){
	if (!document.izracunsvi.dop5mp.checked){
		document.izracunsvi.dop5mp.checked=false;
		prikazi('paketm_vrijednost');
		return false;
	}
	var kojepolje;
	var arr= new Array() 
	var temp = document.getElementById("paketodm").value;
	arr=temp.split("|");
	
	var tippaketa=document.getElementById("izborpaketam").value;
	
	if (tippaketa==1){
		kojepolje=arr[7];
	}else{
		kojepolje=arr[2];
	}
	
	if (kojepolje!="DA"){
		if (lng=="hr") {
				swal('Vrijednosni paket se ne možeposlati u državu odredišta');
			}else{
				swal('Insured parcel not available for destination country.');
			}
		return false;
	}else{
		return true;
	}
}

function provjerapaketprioritet(){
	if (document.izracunsvi.dop5mp.checked){
		var arr= new Array();
		var temp = document.getElementById("paketodm").value;
		arr=temp.split("|");

		if (arr[2]=="null" && arr[1]!="null"){
			swal('Vrijednosni paket mora biti prioritetan za zemlju odredišta');
			return false;
		}else if (arr[2]=="null"){
			swal('Vrijednosni paket se ne može poslati u zemlju odredišta');
			return false;
		}else{
			return true;
		}
		
	}else{
		return false;
	}
}

function izracunajpismom(x){

	tezinapismou = parseFloat(x.tezinapismom.value);

	var temp = tezinapismou;
	
	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0";
	if (a<0)temp=temp+".00";
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}

function izracunajpismomp(x){

	var zona = document.getElementById("pismoodm2").value;
	tezinapismou = parseFloat(x.tezinapismom2.value);

	var temp = tezinapismou;

	/*
	if (zona==1){
		if (tezinapismou==1) temp='11';
		if (tezinapismou==2) temp='20';
		if (tezinapismou==3) temp='37';
		if (tezinapismou==4) temp='62';
		if (tezinapismou==5) temp='98';
		if (tezinapismou==6) temp='136';
	}
	if (zona==2){
		if (tezinapismou==1) temp='13';
		if (tezinapismou==2) temp='23';
		if (tezinapismou==3) temp='41';
		if (tezinapismou==4) temp='69';
		if (tezinapismou==5) temp='104';
		if (tezinapismou==6) temp='141';
	}
	if (zona==3){
		if (tezinapismou==1) temp='15';
		if (tezinapismou==2) temp='25';
		if (tezinapismou==3) temp='44';
		if (tezinapismou==4) temp='73';
		if (tezinapismou==5) temp='109';
		if (tezinapismou==6) temp='145';
	}
	*/
	
	if (tezinapismou==1) temp='14';
	if (tezinapismou==2) temp='23';
	if (tezinapismou==3) temp='38';
	if (tezinapismou==4) temp='58';
	if (tezinapismou==5) temp='83';
	if (tezinapismou==6) temp='112';
	
	temp=parseFloat(temp);
	
	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0";
	if (a<0)temp=temp+".00";
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}

function izracunajdopism(x){

	var temp = '8.6';
	
	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0";
	if (a<0)temp=temp+".00";
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}


function izracunajtisakm(x){
	var plustemp=0;
	var arr= new Array();
	var temp = document.getElementById("tisakodm").value;
	arr=temp.split("|");
	
	tezinapismou = parseFloat(x.tezinatisakm.value);
	

	
	if (arr[0]==1) zona=1;
	if (arr[0]==2) zona=2;
	if (arr[0]==3) zona=3;
	
	/*
	if (zona==1){
		if (tezinapismou==1) {temp='10'; plustemp='1.5'}
		if (tezinapismou==2) {temp='10'; plustemp='4'}
		if (tezinapismou==3) {temp='10'; plustemp='8'}
		if (tezinapismou==4) {temp='17'; plustemp='17'}
		if (tezinapismou==5) {temp='33'; plustemp='23'}
		if (tezinapismou==6) {temp='63'; plustemp='25'}
		if (tezinapismou==7) {temp='108'; plustemp='27'}
	}
	if (zona==2){
		if (tezinapismou==1) {temp='12'; plustemp='2'}
		if (tezinapismou==2) {temp='12'; plustemp='5'}
		if (tezinapismou==3) {temp='12'; plustemp='9'}
		if (tezinapismou==4) {temp='20'; plustemp='18'}
		if (tezinapismou==5) {temp='38'; plustemp='25'}
		if (tezinapismou==6) {temp='72'; plustemp='27'}
		if (tezinapismou==7) {temp='111'; plustemp='29'}
	}
	if (zona==3){
		if (tezinapismou==1) {temp='13'; plustemp='2.8'}
		if (tezinapismou==2) {temp='13'; plustemp='6'}
		if (tezinapismou==3) {temp='13'; plustemp='10'}
		if (tezinapismou==4) {temp='22'; plustemp='19'}
		if (tezinapismou==5) {temp='43'; plustemp='26'}
		if (tezinapismou==6) {temp='78'; plustemp='28'}
		if (tezinapismou==7) {temp='114'; plustemp='30'}
	}
	
	*/
	
	if (tezinapismou==1) {temp='11'; plustemp='9.5'}
	if (tezinapismou==2) {temp='19'; plustemp='18'}
	if (tezinapismou==3) {temp='35'; plustemp='25'}
	if (tezinapismou==4) {temp='65'; plustemp='27'}
	if (tezinapismou==5) {temp='105'; plustemp='29'}
	
	temp = parseFloat(temp);
	
	
	if (x.dop7mt.checked) {
		
		temp= parseFloat(temp) + parseFloat(plustemp); 
	} 	
	
	
	
	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0";
	if (a<0)temp=temp+".00";
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}


function izracunajvrecam(x){
	var plustemp=0;
	var arr= new Array();
	var temp = document.getElementById("vrecaodm").value;
	arr=temp.split("|");
	/*
	if (arr[5]==86){ //Izrael
		swal('M vreća se ne može poslati u zemlju odredišta');
		return false;
	}
	*/
	tezinapismou = parseFloat(x.tezinavrecam.value);
	

	
	if (arr[0]==1) zona=1;
	if (arr[0]==2) zona=2;
	if (arr[0]==3) zona=3;
	
	if (zona==1){
		if (tezinapismou==1) {temp='75';}
		if (tezinapismou==2) {temp='125';}
		if (tezinapismou==3) {temp='210';}
		if (tezinapismou==4) {temp='290';}
		if (tezinapismou==5) {temp='335';}
	}
	if (zona==2){
		if (tezinapismou==1) {temp='90';}
		if (tezinapismou==2) {temp='170';}
		if (tezinapismou==3) {temp='280';}
		if (tezinapismou==4) {temp='410';}
		if (tezinapismou==5) {temp='550';}
	}
	if (zona==3){
		if (tezinapismou==1) {temp='130';}
		if (tezinapismou==2) {temp='200';}
		if (tezinapismou==3) {temp='360';}
		if (tezinapismou==4) {temp='550';}
		if (tezinapismou==5) {temp='720';}
	}

	temp = parseFloat(temp);
	
	if (x.dop7mv.checked) {
		
		if (zona==1){
			if (tezinapismou==1) {plustemp='14';}
			if (tezinapismou==2) {plustemp='21';}
			if (tezinapismou==3) {plustemp='30';}
			if (tezinapismou==4) {plustemp='45';}
			if (tezinapismou==5) {plustemp='60';}
		}
		if (zona==2){
			if (tezinapismou==1) {plustemp='16';}
			if (tezinapismou==2) {plustemp='25';}
			if (tezinapismou==3) {plustemp='40';}
			if (tezinapismou==4) {plustemp='55';}
			if (tezinapismou==5) {plustemp='70';}
		}
		if (zona==3){
			if (tezinapismou==1) {plustemp='20';}
			if (tezinapismou==2) {plustemp='31';}
			if (tezinapismou==3) {plustemp='45';}
			if (tezinapismou==4) {plustemp='60';}
			if (tezinapismou==5) {plustemp='75';}
		}
		
		temp= parseFloat(temp) + parseFloat(plustemp); 
	} 	
		
	

	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0";
	if (a<0)temp=temp+".00";
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}


function izracunajvrecapum(x){
	var plustemp=0;
	var arr= new Array();
	var temp = document.getElementById("vrecapuodm").value;
	arr=temp.split("|");
	
	if (arr[5]==188 || arr[5]==97){ //S.A.D. ,Kanada
		if (lng=="hr") {
			swal('Prioritetna M vreća s potvrđenim uručenjem ne može se poslati u zemlju odredišta');
		}else{
			swal('Priority registered M bag not available for destination country');
		}
		
		return false;
	}
	
	tezinapismou = parseFloat(x.tezinavrecapum.value);
	

	
	if (arr[0]==1) zona=1;
	if (arr[0]==2) zona=2;
	if (arr[0]==3) zona=3;
	
	if (zona==1){
		if (tezinapismou==1) {temp='115';}
		if (tezinapismou==2) {temp='172';}
		if (tezinapismou==3) {temp='266';}
		if (tezinapismou==4) {temp='361';}
		if (tezinapismou==5) {temp='421';}
	}
	if (zona==2){
		if (tezinapismou==1) {temp='132';}
		if (tezinapismou==2) {temp='221';}
		if (tezinapismou==3) {temp='346';}
		if (tezinapismou==4) {temp='491';}
		if (tezinapismou==5) {temp='646';}
	}
	if (zona==3){
		if (tezinapismou==1) {temp='176';}
		if (tezinapismou==2) {temp='257';}
		if (tezinapismou==3) {temp='431';}
		if (tezinapismou==4) {temp='636';}
		if (tezinapismou==5) {temp='821';}
	}
	temp = parseFloat(temp);

	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0";
	if (a<0)temp=temp+".00";
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}

function izracunajpaketm(x){

	var arr= new Array();
	var temp = document.getElementById("paketodm").value;
	arr=temp.split("|");
	
	var izborpaketa = x.izborpaketam.value;
	
	dop2 = parseFloat(x.dop2mp.value);
	dop3 = parseFloat(x.dop3mp.value);
	
	
	
	dop4 = parseFloat(x.dop4mp.value);
	//dop4a = parseFloat(x.dop4mpa.value);
	dop5 = parseFloat(x.dop5mp.value);
	//dop7 = parseFloat(x.dop7mp.value);

	vrijednostpismou = x.vrijednostpaketm.value;
	vrijednostpismou = vrijednostpismou.replace(",",".");
	vrijednostpismou = parseFloat(vrijednostpismou);
	
	var tezpaketam = x.tezpaketam.value;
	tezpaketam = tezpaketam.replace(",",".");
	tezpaketam = parseFloat(tezpaketam);
	
	
	if (izborpaketa==1) {
		if (isNaN(tezpaketam) || (tezpaketam>arr[9])){
			
			if (lng=="hr") {
				swal('Maksimalna masa je '+arr[9]+' kg');
			}else{
				swal('The maximum weight is '+arr[9]+' kg');
			}
			
			
			return false;
		}
		
		if (!x.dop5mp.checked && arr[6]=='NE') {
			
			if (lng=="hr") {
				swal('Samo vrijednosni paket možete poslati u državu odredišta');
			}else{
				swal('Only Insured parcel is available for destination country.');
			}
			return false;
		}
		
	}else{
		if (isNaN(tezpaketam) || (tezpaketam>arr[4])){
			if (lng=="hr") {
				swal('Maksimalna masa je '+arr[4]+' kg');
			}else{
				swal('The maximum weight is '+arr[4]+' kg');
			}
			return false;
		}
		if (!x.dop5mp.checked && arr[1]=='NE') {
			if (lng=="hr") {
				swal('Samo vrijednosni paket možete poslati u državu odredišta');
			}else{
				swal('Only Insured parcel is available for destination country.');
			}
			return false;
		}
	}
	
	
	
	
	
	
	
	tezpaketam2=tezpaketam;
	a = InStr(tezpaketam, '.');
	b= Mid(tezpaketam,a+1,2);
	if (b<10 && a>0) tezpaketam=Mid(tezpaketam,0,a)*1+1;
	
	if (tezpaketam<=1){
		if (izborpaketa==1) {
			if (arr[0]==1) zona=75;
			if (arr[0]==2) zona=120;
			if (arr[0]==3) zona=150;
			if (arr[0]==4) zona=180;
			if (arr[0]==5) zona=90;
			if (arr[0]==6) zona=144;
			if (arr[0]==7) zona=100;
		} else {
			if (arr[0]==1) zona=75;
			if (arr[0]==2) zona=144;
			if (arr[0]==3) zona=174;
			if (arr[0]==4) zona=190;
			if (arr[0]==5) zona=90;
			if (arr[0]==6) zona=144;
			if (arr[0]==7) zona=100;
		}					
	}else if(tezpaketam<=2){
		if (izborpaketa==1) {
			if (arr[0]==1) zona=90;
			if (arr[0]==2) zona=140;
			if (arr[0]==3) zona=180;
			if (arr[0]==4) zona=220;
			if (arr[0]==5) zona=120;
			if (arr[0]==6) zona=188;
			if (arr[0]==7) zona=130;
		} else {
			if (arr[0]==1) zona=90;
			if (arr[0]==2) zona=188;
			if (arr[0]==3) zona=236;
			if (arr[0]==4) zona=300;
			if (arr[0]==5) zona=120;
			if (arr[0]==6) zona=188;
			if (arr[0]==7) zona=130;
		}					
	}else if(tezpaketam<=5){
		if (izborpaketa==1) {
			if (arr[0]==1) zona=110;
			if (arr[0]==2) zona=220;
			if (arr[0]==3) zona=250;
			if (arr[0]==4) zona=320;
			if (arr[0]==5) zona=165;
			if (arr[0]==6) zona=260;
			if (arr[0]==7) zona=180;
		} else {
			if (arr[0]==1) zona=110;
			if (arr[0]==2) zona=260;
			if (arr[0]==3) zona=306;
			if (arr[0]==4) zona=400;
			if (arr[0]==5) zona=165;
			if (arr[0]==6) zona=260;
			if (arr[0]==7) zona=180;
		}					
	}else if(tezpaketam<=10){
		if (izborpaketa==1) {
			if (arr[0]==1) zona=150;
			if (arr[0]==2) zona=270;
			if (arr[0]==3) zona=400;
			if (arr[0]==4) zona=500;
			if (arr[0]==5) zona=230;
			if (arr[0]==6) zona=310;
			if (arr[0]==7) zona=250;
		} else {
			if (arr[0]==1) zona=150;
			if (arr[0]==2) zona=310;
			if (arr[0]==3) zona=480;
			if (arr[0]==4) zona=580;
			if (arr[0]==5) zona=230;
			if (arr[0]==6) zona=310;
			if (arr[0]==7) zona=250;
		}					
	}else if(tezpaketam<=15){
		if (izborpaketa==1) {
			if (arr[0]==1) zona=240;
			if (arr[0]==2) zona=280;
			if (arr[0]==3) zona=440;
			if (arr[0]==4) zona=520;
			if (arr[0]==5) zona=350;
			if (arr[0]==6) zona=400;
			if (arr[0]==7) zona=320;
		} else {
			if (arr[0]==1) zona=240;
			if (arr[0]==2) zona=320;
			if (arr[0]==3) zona=520;
			if (arr[0]==4) zona=600;
			if (arr[0]==5) zona=350;
			if (arr[0]==6) zona=400;
			if (arr[0]==7) zona=320;
		}					
	}else if(tezpaketam<=20){
		if (izborpaketa==1) {
			if (arr[0]==1) zona=280;
			if (arr[0]==2) zona=320;
			if (arr[0]==3) zona=520;
			if (arr[0]==4) zona=640;
			if (arr[0]==5) zona=415;
			if (arr[0]==6) zona=450;
			if (arr[0]==7) zona=360;
		} else {
			if (arr[0]==1) zona=280;
			if (arr[0]==2) zona=360;
			if (arr[0]==3) zona=600;
			if (arr[0]==4) zona=680;
			if (arr[0]==5) zona=415;
			if (arr[0]==6) zona=450;
			if (arr[0]==7) zona=360;
		}					
	}else if(tezpaketam<=25){
		if (izborpaketa==1) {
			if (arr[0]==1) zona=320;
			if (arr[0]==2) zona=400;
			if (arr[0]==3) zona=600;
			if (arr[0]==4) zona=720;
			if (arr[0]==5) zona=500;
			if (arr[0]==6) zona=550;
			if (arr[0]==7) zona=440;
		} else {
			if (arr[0]==1) zona=320;
			if (arr[0]==2) zona=440;
			if (arr[0]==3) zona=680;
			if (arr[0]==4) zona=760;
			if (arr[0]==5) zona=500;
			if (arr[0]==6) zona=550;
			if (arr[0]==7) zona=440;
		}					
	}else if(tezpaketam<=30){
		if (izborpaketa==1) {
			if (arr[0]==1) zona=360;
			if (arr[0]==2) zona=480;
			if (arr[0]==3) zona=680;
			if (arr[0]==4) zona=800;
			if (arr[0]==5) zona=610;
			if (arr[0]==6) zona=650;
			if (arr[0]==7) zona=520;
		} else {
			if (arr[0]==1) zona=360;
			if (arr[0]==2) zona=520;
			if (arr[0]==3) zona=760;
			if (arr[0]==4) zona=880;
			if (arr[0]==5) zona=610;
			if (arr[0]==6) zona=650;
			if (arr[0]==7) zona=520;
		}					
	}
	
	
	
	
	var temp = zona;
	var dodataknavrijednost=0;
	
	if(tezpaketam<=10){
		dop2=6;
		dop3=6.4;
	}
	
	var a;
	
	if (x.dop3mp.checked){
		
		if (izborpaketa==1){
			a=arr[10];
		}else{
			a=arr[5];
		}
		
		if (a.indexOf('AR')==-1){
			
			if (lng=="hr") {
				swal('Usluga povratnice nije moguća');
			}else{
				swal('Advice of delivery (AR)) not available');
			}
			return false;
		}
		temp=temp+dop3;
	} 
	
	if (x.dop2mp.checked) {
		var aaa = arr[11];

		if (aaa=='0' || aaa=='' || aaa=='null'){
			if (lng=="hr") {
				swal('Otkupnina nije moguća');
			}else{
				swal('Cash-on-delivery (COD) not available');
			}
			
			return false;
		}
		temp=temp+dop2;
	} 
	
		
	
	
	var temp1 = vrijednostpismou*0.1104;
	if (x.dop5mp.checked && isNaN(vrijednostpismou)){
		swal('Upišite vrijednost pošiljke')
		return false;
	}
	
	if (x.dop5mp.checked){ //ako se racuna po DTSu
		
		if (vrijednostpismou>10000){
			swal('Maksimalna vrijednost pošiljke je 10.000 kn')
		return false;
		}
		
		if(tezpaketam<=10){
			if (vrijednostpismou>1600 && vrijednostpismou<=3000){
				dodataknavrijednost=44;
			}else if (vrijednostpismou>3000 && vrijednostpismou<=5000){
				dodataknavrijednost=76;
			}else if (vrijednostpismou>5000 && vrijednostpismou<=7000){
				dodataknavrijednost=104;
			}else if (vrijednostpismou>7000 && vrijednostpismou<=10000){
				dodataknavrijednost=148;
			}
			
			
		}else{
			if (vrijednostpismou>1600 && vrijednostpismou<=3000){
				dodataknavrijednost=55;
			}else if (vrijednostpismou>3000 && vrijednostpismou<=5000){
				dodataknavrijednost=95;
			}else if (vrijednostpismou>5000 && vrijednostpismou<=7000){
				dodataknavrijednost=130;
			}else if (vrijednostpismou>7000 && vrijednostpismou<=10000){
				dodataknavrijednost=185;
			}
		}
		

		temp=temp+dodataknavrijednost;
		
		
	}
	
	/*	
	if (x.dop5mp.checked){ //ako se racuna po DTSu
		
		if (izborpaketa==1){
			a=arr[8];
		}else{
			a=arr[3];
		}
		
		
		if (Math.round(temp1)>a){
			var dozvola=Math.round(a*9.0574);
			swal('Dozvoljena vrijednost je '+dozvola+' kn');
			return false;
		}

		//1 DTS = 9.0574
		//1kn   = 1/9.0574 = 0.1104
		//65 DTS=588.974
		
		var dts=588.974;
		if ((vrijednostpismou % dts)>0){
			var mod=vrijednostpismou % dts;
			vrijednostpismou=vrijednostpismou-mod+dts
		
		}

		temp=temp+(vrijednostpismou*0.1104/65*4);
		temp=temp+18;
		
		
	}
	*/
	
	//var temp2 = (temp*50/100);
	//if (x.dop4mp.checked) temp=temp+temp2;
	
	var lomglo=35;
	
	if(tezpaketam<=10){
		//lomglo=14.4;
	}
	
	if (x.dop4mp.checked) temp=temp+dop4;
	//if (x.dop4mpa.checked) temp=temp+lomglo;
	
	//if (x.dop4mp.checked && x.dop4mpa.checked) temp=temp-lomglo;
	
	var temp3 = (temp*50/100);
	
	//if (x.dop7mp.checked) temp=temp+temp3;
	
	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0";
	if (a<0)temp=temp+".00";
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}





function izracunajprepom(x){
	var plustemp=0;
	var arr= new Array();
	var temp = document.getElementById("prepoodm").value;
	arr=temp.split("|");
	
	tezinapismou = parseFloat(x.tezinaprepom.value);
	
	
	dop4 = parseFloat(x.dop4pm.value);
	//dop5 = parseFloat(x.dop5pm.value);
	  

	var temp = tezinapismou;
	
	
	if (arr[0]==1) zona=1;
	if (arr[0]==2) zona=2;
	if (arr[0]==3) zona=3;

	/*
	if (zona==1){
		if (tezinapismou==1) temp='26.8';
		if (tezinapismou==2) temp='26.8'; 
		if (tezinapismou==3) temp='36.6';
		if (tezinapismou==4) temp='59.6';
		if (tezinapismou==5) temp='92.4' ;
		if (tezinapismou==6) temp='130';
		if (tezinapismou==7) temp='167.6';
	}
	if (zona==2){
		if (tezinapismou==1) temp='27.6';
		if (tezinapismou==2) temp='27.6';
		if (tezinapismou==3) temp='37.4';
		if (tezinapismou==4) temp='60.4';
		if (tezinapismou==5) temp='94';
		if (tezinapismou==6) temp='131.6';
		if (tezinapismou==7) temp='169.2';
	}
	if (zona==3){
		if (tezinapismou==1) temp='28.4';
		if (tezinapismou==2) temp='28.4';
		if (tezinapismou==3) temp='38.2';
		if (tezinapismou==4) temp='61.2';
		if (tezinapismou==5) temp='94.8';
		if (tezinapismou==6) temp='132.4';
		if (tezinapismou==7) temp='170';
	}
	*/
	
	if (tezinapismou==1) temp='32';
	if (tezinapismou==2) temp='40';
	if (tezinapismou==3) temp='60';
	if (tezinapismou==4) temp='82';
	if (tezinapismou==5) temp='105';
	if (tezinapismou==6) temp='135';
	
	temp= parseFloat(temp);
	
	if (x.dop4pm.checked){
		var a = arr[1];
		if (a.indexOf('AR')==-1){
			if (lng=="hr") {
				swal('Usluga povratnice nije moguća');
			}else{
				swal('Advice of delivery (AR)) not available');
			}
			return false;
		}
		temp=temp+dop4;
	} 
	//if (x.dop5m.checked) temp=temp+dop5;
	//trenutno stavljeno
	/*
	if (x.dop5pm.checked) {
		var a = arr[2];

		if (a=='0' || a=='' || a=='null'){
			if (lng=="hr") {
				swal('Otkupnina nije moguća');
			}else{
				swal('Cash-on-delivery (COD) not available');
			}
			return false;
		}
		temp=temp+dop5;;
	} 
	*/


	
	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0";
	if (a<0)temp=temp+".00";
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}




function izracunajvrijedm(x){
	var plustemp=0;
	var arr= new Array();
	var temp = document.getElementById("vrijedodm").value;
	arr=temp.split("|");
	
	tezinapismou = parseFloat(x.tezinavrijedm.value);
	
	var temp = tezinapismou;
	
	
	if (arr[0]==1) zona=1;
	if (arr[0]==2) zona=2;
	if (arr[0]==3) zona=3;
	
	/*
	if (zona==1){
		if (tezinapismou==1) temp=31.3;
		if (tezinapismou==2) temp=31.3;
		if (tezinapismou==3) temp=41.1;
		if (tezinapismou==4) temp=64.1;
		if (tezinapismou==5) temp=96.9;
		if (tezinapismou==6) temp=134.5;
		if (tezinapismou==7) temp=171.6;
	}
	if (zona==2){
		if (tezinapismou==1) temp=32.1;
		if (tezinapismou==2) temp=32.1;
		if (tezinapismou==3) temp=41.9;
		if (tezinapismou==4) temp=64.9;
		if (tezinapismou==5) temp=98.5;
		if (tezinapismou==6) temp=136.1;
		if (tezinapismou==7) temp=173.2;
	}
	if (zona==3){
		if (tezinapismou==1) temp=32.9;
		if (tezinapismou==2) temp=32.9;
		if (tezinapismou==3) temp=42.7;
		if (tezinapismou==4) temp=65.7;
		if (tezinapismou==5) temp=99.3;
		if (tezinapismou==6) temp=136.9;
		if (tezinapismou==7) temp=174;
	}
	*/
	
	if (tezinapismou==1) temp='55.6';
	if (tezinapismou==2) temp='63.6';
	if (tezinapismou==3) temp='83.6';
	if (tezinapismou==4) temp='106.4';
	if (tezinapismou==5) temp='129.4';
	if (tezinapismou==6) temp='159.4';
	
	temp=parseFloat(temp);

	dop4 = parseFloat(x.dop4vm.value);
	//dop5 = parseFloat(x.dop5vm.value);
	
	
	vrijednostpismou = x.vrijednostvm.value;
	vrijednostpismou = vrijednostpismou.replace(",",".");
	vrijednostpismou = parseFloat(vrijednostpismou);
	
	
	
	if (x.dop4vm.checked){
		var a = arr[1];
		if (a.indexOf('AR')==-1){
			if (lng=="hr") {
				swal('Usluga povratnice nije moguća');
			}else{
				swal('Advice of delivery (AR)) not available');
			}
			return false;
		}
		temp=temp+dop4;
	} 
	//if (x.dop5m.checked) temp=temp+dop5;
	//trenutno stavljeno
	/*
	if (x.dop5vm.checked) {
		var a = arr[2];

		if (a=='0' || a=='' || a=='null'){
			if (lng=="hr") {
				swal('Otkupnina nije moguća');
			}else{
				swal('Cash-on-delivery (COD) not available');
			}
			return false;
		}
		temp=temp+dop5;;
	} 
	*/

	var temp1 = vrijednostpismou*0.1104;
	
	

	if (Math.round(temp1)>arr[4]){
		var dozvola=Math.round(arr[4]*9.0574);
		swal('Dozvoljena vrijednost je '+dozvola+' kn');
		return false;
	}
	
	
	var dts=601.744;
	if ((vrijednostpismou % dts)>0){
		var mod=vrijednostpismou % dts;
		vrijednostpismou=vrijednostpismou-mod+dts
	
	}
	
	temp=temp+(vrijednostpismou*0.1080/65*4);
		
	1080
	
	//if (x.dop6m.checked) temp=temp+dop6;
	
	
	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0";
	if (a<0)temp=temp+".00";
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}



function stasvesmije(x){
	
	var arr= new Array();
	var temp = document.getElementById("paketodm").value;
	arr=temp.split("|");
	
	var tippaketa=document.getElementById("izborpaketam").value;
	var a;
	
	
	if (tippaketa==1){
		a=arr[10];
	}else{
		a=arr[5];
	}

	
	if (arr[0]=="1" || arr[0]=="5" || arr[0]=="6" || arr[0]=="7" || a.indexOf('NE')>-1){
		$('#izborpaketam').html("<option value='2'>premium</option>").selectpicker('refresh');

	}else{
		$('#izborpaketam').html("<option value='1'>ekonomična</option><option value='2'>premium</option>").selectpicker('refresh');
	}
	
	
	if (a.indexOf('AR')==-1){
		x.dop3mp.checked=false;
		document.getElementById('povratnicamp').style.visibility='hidden';
	}else{
		x.dop3mp.checked=false;
		document.getElementById('povratnicamp').style.visibility='visible';
	}
	
	//lomljvo
	if (a.indexOf('L')==-1 && a.indexOf('G')==-1){
		x.dop4mp.checked=false;
		document.getElementById('lomljiviamp').style.visibility='hidden';
	}else{
		x.dop4mp.checked=false;
		document.getElementById('lomljiviamp').style.visibility='visible';
	}
	/*
	//lomljvo
	if (a.indexOf('G')==-1){
		x.dop4mpa.checked=false;
		document.getElementById('glomazniamp').style.visibility='hidden';
	}else{
		x.dop4mpa.checked=false;
		document.getElementById('glomazniamp').style.visibility='visible';
	}
	*/
	if (arr[11]=="" || arr[11]=="0"){
		document.getElementById('otkupninamp').style.visibility='hidden';
	}else{
		document.getElementById('otkupninamp').style.visibility='visible';
	}



		
	
}

 function showotkupnina(val){
 	/*
	arr=val.split("|");
 	
 	if (arr[2]=="175"){
 		document.getElementById('otkupninaems').style.visibility='visible';
 	}else{
 		document.getElementById('otkupninaems').style.visibility='hidden';
 	}
 	*/
 }

 function addOption(selectbox,text,value ){
		var optn = document.createElement("OPTION");
		optn.text = text;
		optn.value = value;
		document.getElementById(selectbox).options.add(optn);
	}


function izracunajemsm(x){
	var arr= new Array();
	var emspaketam = x.emspaketam.value;
	var ppupm = x.ppupm.value;
	var dop2ems = parseFloat(x.dop2ems.value);
	
	emsodm = x.emsodm.value;
	arr=emsodm.split("|");
	
	emsodm=arr[0];
	emspaketamasa=arr[1];
	if (isNaN(emspaketam) || emspaketam>(emspaketamasa.replace(',','.'))*1000){
		swal('Maksimalna masa je ' + emspaketamasa + ' kg');
		return false;
	}
	
	var temp;
		
	if (emspaketam<=200) {
		if (emsodm=='EU1'){temp=164;}
		if (emsodm=='EU'){temp=224;}
		if (emsodm=='AMERIKA'){temp=260;}
		if (emsodm=='SVIJET'){temp=264;}
	}else if (emspaketam>200 && emspaketam<=500){
		if (emsodm=='EU1'){temp=184;}
		if (emsodm=='EU'){temp=244;}
		if (emsodm=='AMERIKA'){temp=274;}
		if (emsodm=='SVIJET'){temp=294;}
	}else if (emspaketam>500 && emspaketam<=1000){
		if (emsodm=='EU1'){temp=214;}
		if (emsodm=='EU'){temp=254;}
		if (emsodm=='AMERIKA'){temp=310;}
		if (emsodm=='SVIJET'){temp=314;}
	}else if (emspaketam>1000 && emspaketam<=2000){
		if (emsodm=='EU1'){temp=224;}
		if (emsodm=='EU'){temp=344;}
		if (emsodm=='AMERIKA'){temp=444;}
		if (emsodm=='SVIJET'){temp=524;}
	}else if (emspaketam>2000 && emspaketam<=5000){
		if (emsodm=='EU1'){temp=264;}
		if (emsodm=='EU'){temp=394;}
		if (emsodm=='AMERIKA'){temp=524;}
		if (emsodm=='SVIJET'){temp=644;}
	}else if (emspaketam>5000){
		if (emsodm=='EU1'){temp=324;}
		if (emsodm=='EU'){temp=454;}
		if (emsodm=='AMERIKA'){temp=684;}
		if (emsodm=='SVIJET'){temp=894;}
	}
		
		
	if (emspaketam>10000){
		if (emsodm=='EU1'){cijena2=6;}
		
		if (emsodm=='EU'){cijena2=8;}
		if (emsodm=='AMERIKA'){cijena2=15;}
		if (emsodm=='SVIJET'){cijena2=20;}
		
		temp1=emspaketam-10000;
		temp2=temp1/500
		
		if ((temp2 % 1)>0) temp2=temp2-(temp2 % 1)+1;
		temp=temp+(Math.round(temp2)*cijena2);
	}
	
	
	if (x.ppupm.value=="2"){temp=temp-44}
	
	if (x.dop2ems.checked) {
		temp=temp+dop2ems;
	} 
	
	
	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0";
	if (a<0)temp=temp+".00";
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}



function izracunajemsm2(x){
	var arr= new Array();
	var emspaketam = x.emspaketam.value;
	var ppupm = x.ppupm.value;
	var dop2ems = parseFloat(x.dop2ems.value);
	
	emsodm = x.emsodm.value;
	arr=emsodm.split("|");
	
	emsodm=arr[0];
	emspaketamasa=arr[1];
	if (isNaN(emspaketam) || emspaketam>(emspaketamasa.replace(',','.'))*1000){
		swal('Maksimalna masa je ' + emspaketamasa + ' kg');
		return false;
	}
	
	var temp;
		
	if (emspaketam<=200) {
		if (emsodm=='EU1'){temp=125;}
		if (emsodm=='EU2'){temp=185;}
		if (emsodm=='EUROPA1'){temp=131;}
		if (emsodm=='EUROPA2'){temp=179;}
		if (emsodm=='AMERIKA'){temp=207.8;}
		if (emsodm=='SVIJET'){temp=211;}
	}else if (emspaketam>200 && emspaketam<=500){
		if (emsodm=='EU1'){temp=140;}
		if (emsodm=='EU2'){temp=210;}
		if (emsodm=='EUROPA1'){temp=147;}
		if (emsodm=='EUROPA2'){temp=195;}
		if (emsodm=='AMERIKA'){temp=219;}
		if (emsodm=='SVIJET'){temp=235;}
	}else if (emspaketam>500 && emspaketam<=1000){
		if (emsodm=='EU1'){temp=162.5;}
		if (emsodm=='EU2'){temp=247.5;}
		if (emsodm=='EUROPA1'){temp=171;}
		if (emsodm=='EUROPA2'){temp=203;}
		if (emsodm=='AMERIKA'){temp=247.8;}
		if (emsodm=='SVIJET'){temp=251;}
	}else if (emspaketam>1000 && emspaketam<=2000){
		if (emsodm=='EU1'){temp=170;}
		if (emsodm=='EU2'){temp=260;}
		if (emsodm=='EUROPA1'){temp=179;}
		if (emsodm=='EUROPA2'){temp=275;}
		if (emsodm=='AMERIKA'){temp=355;}
		if (emsodm=='SVIJET'){temp=419;}
	}else if (emspaketam>2000 && emspaketam<=5000){
		if (emsodm=='EU1'){temp=200;}
		if (emsodm=='EU2'){temp=310;}
		if (emsodm=='EUROPA1'){temp=211;}
		if (emsodm=='EUROPA2'){temp=315;}
		if (emsodm=='AMERIKA'){temp=419;}
		if (emsodm=='SVIJET'){temp=515;}
	}else if (emspaketam>5000){
		if (emsodm=='EU1'){temp=245;}
		if (emsodm=='EU2'){temp=385;}
		if (emsodm=='EUROPA1'){temp=259;}
		if (emsodm=='EUROPA2'){temp=363;}
		if (emsodm=='AMERIKA'){temp=555;}
		if (emsodm=='SVIJET'){temp=715;}
	}
		
		
	if (emspaketam>10000){
		if (emsodm=='EU1'){cijena2=5;}
		if (emsodm=='EU2'){cijena2=7;}
		if (emsodm=='EUROPA1'){cijena2=4.8;}
		if (emsodm=='EUROPA2'){cijena2=6.4;}
		if (emsodm=='AMERIKA'){cijena2=12;}
		if (emsodm=='SVIJET'){cijena2=16;}
		
		temp1=emspaketam-10000;
		temp2=temp1/500
		
		if ((temp2 % 1)>0) temp2=temp2-(temp2 % 1)+1;
		temp=temp+(Math.round(temp2)*cijena2);
	}
	
	
	if (x.ppupm.value=="2"){temp=temp-35}
	
	if (x.dop2ems.checked) {
		temp=temp+dop2ems;
	} 
	
	
	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0";
	if (a<0)temp=temp+".00";
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
}

function napuniselect(x,kojiselect){
	 
	if (x.ppupu.value=='2'){
		$('#'+kojiselect).html("<option value='1'>0 - 0,2 kg</option><option value='2'>iznad 0,2 - 1 kg</option><option value='3'>iznad 1 - 2 kg</option><option value='4'>iznad 2 - 5 kg</option><option value='5'>iznad 5 - 10 kg</option><option value='6'>iznad 10 - 15 kg</option><option value='7'>iznad 15 - 20 kg</option>").selectpicker('refresh');
	}else{
		$('#'+kojiselect).html("<option value='1'>0 - 0,2 kg</option><option value='2'>iznad 0,2 - 1 kg</option><option value='3'>iznad 1 - 2 kg</option><option value='4'>iznad 2 - 5 kg</option><option value='5'>iznad 5 - 10 kg</option><option value='6'>iznad 10 - 15 kg</option><option value='7'>iznad 15 - 20 kg</option><option value='8'>iznad 20 - 25 kg</option><option value='9'>iznad 25 - 30 kg</option><option value='10'>znad 30 - 35 kg</option><option value='11'>znad 35 - 40 kg</option><option value='12'>znad 40 - 45 kg</option><option value='13'>znad 45 - 50 kg</option>").selectpicker('refresh');
	}
	
}

function napuniselect24(x,kojiselect){
	 
	if (x.ppupu.value=='2'){
		$('#'+kojiselect).html("<option value='1'>0 - 1 kg</option><option value='2'>iznad 1 - 2 kg</option><option value='3'>iznad 2 - 5 kg</option><option value='4'>iznad 5 - 10 kg</option><option value='5'>iznad 10 - 15 kg</option><option value='6'>iznad 15 - 20 kg</option>").selectpicker('refresh');
	}else{
		$('#'+kojiselect).html("<option value='1'>0 - 1 kg</option><option value='2'>iznad 1 - 2 kg</option><option value='3'>iznad 2 - 5 kg</option><option value='4'>iznad 5 - 10 kg</option><option value='5'>iznad 10 - 15 kg</option><option value='6'>iznad 15 - 20 kg</option><option value='7'>iznad 20 - 30 kg</option>").selectpicker('refresh');
	}
	
}




function izracunajpaket24(x){
	var cijena;
	var volummasa;
	var preko30;
	var vrijednosthpnadoplata;
	tezinapaketu = parseFloat(x.tezinapaketu1.value);
	dop1 = parseFloat(x.dop1c.value);
	dop2 = parseFloat(x.dop2c.value);
	dop3 = parseFloat(x.dop3c.value);
	//dop4 = parseFloat(x.dop4c.value);
	dop5 = parseFloat(x.dop5c.value);
	dop6 = parseFloat(x.dop6c.value);
	//dop7 = parseFloat(x.dop7c.value);
	//dop8 = parseFloat(x.dop8c.value);
	//dop9 = parseFloat(x.dop9c.value);
	paketuod = x.paketuod1.value;
	paketudo = x.paketudo1.value;
	rokuruc = parseFloat(x.rokuruc.value);
	vrstauputnice= parseFloat(x.vrstauputnice.value);
	
	duzina = parseFloat(x.duzina.value);
	sirina = parseFloat(x.sirina.value);
	visina = parseFloat(x.visina.value);
	
	izbormase = x.izbormase.value;
	
	//var arrpaketuod,arrpaketudo;
	//arrpaketuod=paketuod.split("|");
	//arrpaketudo=paketudo.split("|");
	
	vrijednosthp = x.vrijednosthp.value;
	vrijednosthp = vrijednosthp.replace(",",".");
	vrijednosthp = parseFloat(vrijednosthp);
	
	otkupninahp = x.otkupninahp.value;
	otkupninahp = otkupninahp.replace(",",".");
	otkupninahp = parseFloat(otkupninahp);
	
	
	if (isNaN(vrijednosthp)){
		swal('Upišite vrijednost pošiljke')
		return false;
	}
	
	if (vrijednosthp>100000){
		swal('Maksimalna vrijednost pošiljke je 100000,00 kn');
		return false;
	}
	if (x.dop2c.checked) {
		if (isNaN(otkupninahp)){
			swal('Upišite otkupninu')
			return false;
		}
		
		if (otkupninahp>25000){
			swal('Maksimalna otkupnina je 25.000,00 kn');
			return false;
		}
	}
	if(duzina>0 && sirina>0 && visina>0 && izbormase=='izbormase2'){
		volummasa=duzina*sirina*visina/5000
		//ako je Pošiljka predana u poštanskom uredu onda je 20 kg max inace 30
		if (x.ppupu.value=='2'){
			if (volummasa<=1){
				tezinapaketu=1;
			}else if (volummasa<=2){
				tezinapaketu=2;
			}else if (volummasa<=5){
				tezinapaketu=3;
			}else if (volummasa<=10){
				tezinapaketu=4;
			}else if (volummasa<=15){
				tezinapaketu=5;
			}else if (volummasa<=20){
				tezinapaketu=6;
			}else{
				tezinapaketu=6;
				preko30=volummasa-20;
			}
		}else{
			if (volummasa<=1){
				tezinapaketu=1;
			}else if (volummasa<=2){
				tezinapaketu=2;
			}else if (volummasa<=5){
				tezinapaketu=3;
			}else if (volummasa<=10){
				tezinapaketu=4;
			}else if (volummasa<=15){
				tezinapaketu=5;
			}else if (volummasa<=20){
				tezinapaketu=6;
			}else if (volummasa<=30){
				tezinapaketu=7;
			}else{
				tezinapaketu=7;
				preko30=volummasa-30;
			}
		}
		
		
	}
	
	
	if (tezinapaketu==1){
		cijena=39;
	}else if (tezinapaketu==2){
		cijena=44;
	}else if (tezinapaketu==3){
		cijena=49;
	}else if (tezinapaketu==4){
		cijena=53;
	}else if (tezinapaketu==5){
		cijena=69;
	}else if (tezinapaketu==6){
		cijena=83;
	}else if (tezinapaketu==7){
		cijena=103;
	}
	
	
	if (x.ppupu.value=='2'){cijena=cijena*0.9;}
	
	
	
	
	temp=cijena;
	
	
	
	//za svakih 10 kg preko 30 kg uvecava se cijena za 10kn
	if (preko30>0){
		var a = preko30/5;
		if ((preko30 % 5)>0) a=a+1;
		temp=temp+(Math.round(a)*10);
	}
	
	
	if(paketuod=='' || paketudo=='' || x.dopd1.value=='0') temp +=12.5;
	
	if (x.dop9h.checked) temp=temp+32.5;
	if (x.dop11h.checked) temp=temp+17.5;
	
	if (x.dop1c.checked) temp=temp+dop1;
	if (x.dop2c.checked) {
		temp=temp+dop2;
		
		if (vrstauputnice==1){
			if(otkupninahp<=273.22){
				temp=temp+5.0;
			}else if (otkupninahp<=4371.59){
				temp=temp+(otkupninahp*0.0183);
			}else{
				temp=temp+80.0;
			}
		}else{
			if(otkupninahp<=240.00){
				temp=temp+9.0;
			}else{
				temp=temp+(otkupninahp*0.0375);
			}
		}
		
	}
	if (x.dop5c.checked) temp=temp+dop5;
	if (x.dop6c.checked) temp=temp+dop6;

	if (x.dop3c.checked) temp=temp+(cijena*50/100);
	/*
	if (vrijednosthp>500) {
		vrijednosthp=vrijednosthp-500;
		vrijednosthpnadoplata=(vrijednosthp*0.25/100)
		temp=temp+vrijednosthpnadoplata; 
	} 
	*/
	if (vrijednosthp>500 && vrijednosthp<=2100) {
		temp=temp+5.0; 
	}else if (vrijednosthp>2100) {
		vrijednosthpnadoplata=(vrijednosthp*0.25/100)
		temp=temp+vrijednosthpnadoplata; 
	} 
	
	temp = Math.round(temp*100)/100;
	a = InStr(temp, '.');
	b= Mid(temp,a+1,2);
	if (b<10 && a>0 && b.length<2) temp=temp+"0";
	if (a<0)temp=temp+".00";
	temp=temp.toString();
	$('#rezultat')[0].innerHTML = temp.replace('.',',');
	napunieuro();
	changeDiv('ukupno','block');
}