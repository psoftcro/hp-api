import uvicorn

from src.config import config


def run():
    uvicorn.run(
        "src.app:application",
        host=config.HOST,
        port=config.PORT,
        reload=config.DEBUG,
        reload_dirs=["src"],
    )


if __name__ == "__main__":
    run()
