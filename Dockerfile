FROM python:3.8-alpine

COPY . /app
WORKDIR /app

RUN apk update && \
    apk add tzdata && \
    apk add --no-cache --virtual=.build-deps gcc libffi-dev build-base && \
    apk add --no-cache --virtual=.run-deps && \
    pip install --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt && \
    apk --purge del .build-deps

RUN cp /usr/share/zoneinfo/Europe/Zagreb /etc/localtime
RUN echo "Europe/Zagreb" > /etc/timezone
ENV TZ="Europe/Zagreb"

ENTRYPOINT ["./run.sh"]
CMD ["api"]
EXPOSE 5000
