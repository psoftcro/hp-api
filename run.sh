#!/bin/sh

if [ "$1" = "api" ]
then
    gunicorn 'src.app:application' --config .misc/gunicorn_config.py
elif [ "$1" = "worker" ]
then
    python worker.py
else
    exec "$@"
fi
