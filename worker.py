import logging

from apscheduler.schedulers.blocking import BlockingScheduler

from src.helpers.currency import refresh_course_list
from src.helpers.redis import RedisJobStore
from src.helpers.version import check_updates

stdio_handler = logging.StreamHandler()
stdio_handler.setLevel(logging.INFO)
logger = logging.getLogger("apscheduler.executors.default")
logger.addHandler(stdio_handler)
logger.setLevel(logging.DEBUG)
store = RedisJobStore()
scheduler = BlockingScheduler()


scheduler.add_jobstore(
    store,
    jobs_key="hp.api.worker.jobs",
    run_times_key="hp.api.worker.run_times",
)

scheduler.add_job(
    refresh_course_list,
    "interval",
    id="refresh_course_list",
    hours=1,
    replace_existing=True,
)

scheduler.add_job(
    check_updates,
    "interval",
    id="check_updates",
    days=1,
    replace_existing=True,
)

try:
    scheduler.start()
except (KeyboardInterrupt, SystemExit):
    pass
