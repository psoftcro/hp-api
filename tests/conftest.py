import pytest
from fastapi.testclient import TestClient

from src.app import application as _app, configure_app


@pytest.fixture(scope="session")
def app():
    configure_app(_app)
    client = TestClient(_app)
    return client
