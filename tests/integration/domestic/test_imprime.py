from random import choice

from src.helpers.domestic.imprime import (
    calculate_imprime_cost,
    imprime_weight_mapper,
    imprime_additional_sum_mapper,
)


weight_list = list(imprime_weight_mapper.keys())
random_weight = choice(weight_list)
max_weight = weight_list[-1]
additional_sum_list = list(imprime_additional_sum_mapper.keys())
random_additional_sum = choice(additional_sum_list)


def test_calculate_imprime_success(app):
    params = {"weight": random_weight, "additional": random_additional_sum}
    response = app.get("/domestic/imprime", params=params)
    data, code = response.json(), response.status_code

    assert code == 200 and data.get("value") == calculate_imprime_cost(**params)


def test_calculate_imprime_fail(app):
    params = {"weight": max_weight + 1}
    response = app.get("/domestic/imprime", params=params)
    data, code = response.json(), response.status_code

    assert code == 422
