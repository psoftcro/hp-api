from random import choice, uniform

from src.helpers.domestic.parcel import (
    calculate_parcel_cost,
    parcel_weight_mapper,
    parcel_additional_mul_mapper,
    parcel_additional_sum_mapper,
    parcel_min_denoted_value,
    parcel_max_denoted_value,
)


weight_list = list(parcel_weight_mapper.keys())
random_weight = choice(weight_list)
max_weight = weight_list[-1]
random_location = choice(
    list(parcel_weight_mapper.get(random_weight).get("delivery_location").keys())
)
random_denoted_value = round(
    uniform(parcel_min_denoted_value, parcel_max_denoted_value)
)
random_additional_list = choice(
    list(parcel_additional_sum_mapper.keys())
    + list(parcel_additional_mul_mapper.keys())
)


def test_parcel_success(app):
    params = {
        "weight": random_weight,
        "delivery_location": random_location,
        "denoted_value": random_denoted_value,
        "additional": random_additional_list,
    }
    response = app.get("/domestic/parcel", params=params)
    data, code = response.json(), response.status_code

    assert code == 200 and data.get("value") == calculate_parcel_cost(**params)


def test_parcel_fail(app):
    params = {"weight": max_weight + 1}
    response = app.get("/domestic/parcel", params=params)
    data, code = response.json(), response.status_code

    assert code == 422
