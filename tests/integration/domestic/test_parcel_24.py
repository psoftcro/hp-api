from random import choice, uniform

from src.helpers.domestic.parcel_24 import calculate_parcel_24_cost
from src.helpers.domestic.parcel_24.mapper import (
    parcel_24_weight_mapper,
    parcel_24_city_mapper,
    parcel_24_additional_mul_mapper,
    parcel_24_additional_sum_mapper,
    parcel_24_min_length,
    parcel_24_max_length,
    parcel_24_min_width,
    parcel_24_max_width,
    parcel_24_min_height,
    parcel_24_max_height,
    parcel_24_min_denoted_value,
    parcel_24_max_denoted_value,
    parcel_24_max_dimensions,
    parcel_24_money_wire_mapper,
    parcel_24_max_ransom,
    parcel_24_min_ransom,
)


def _get_limit(current_limit, subtrahend, min_limit):
    current_limit -= subtrahend

    return current_limit if current_limit >= min_limit else min_limit


random_delivery_time = None
random_weight = None
random_length = None
random_width = None
random_height = None
random_ransom = None
random_money_wire = None

chance = round(uniform(1, 2))
if chance == 2:
    limit = parcel_24_max_dimensions - (
        parcel_24_min_height + parcel_24_min_length + parcel_24_min_width
    )
    random_weight = None
    random_length = round(uniform(parcel_24_min_length, parcel_24_max_length))
    limit = _get_limit(limit, random_length, parcel_24_min_length)
    random_width = round(
        uniform(
            parcel_24_min_width,
            parcel_24_max_width if parcel_24_max_width < limit else limit,
        )
    )
    limit = _get_limit(limit, random_width, parcel_24_min_width)
    random_height = round(
        uniform(
            parcel_24_min_height,
            parcel_24_max_height if parcel_24_max_height < limit else limit,
        )
    )

else:
    weight_list = list(parcel_24_weight_mapper.keys())
    random_weight = choice(weight_list)


weight_limit = list(parcel_24_weight_mapper.keys())[-1]

location_list = list(
    parcel_24_weight_mapper.get(next(iter(parcel_24_weight_mapper)))
    .get("delivery_location")
    .keys()
)
delivery_time_list = list(
    parcel_24_weight_mapper.get(next(iter(parcel_24_weight_mapper)))
    .get("delivery_time")
    .keys()
)
random_location = choice(location_list)
random_delivery_time = choice(delivery_time_list)
city_list = list(parcel_24_city_mapper.keys())
random_city_from = choice(city_list)
random_city_to = choice(city_list)
random_denoted_value = round(
    uniform(parcel_24_min_denoted_value, parcel_24_max_denoted_value)
)
additional_sum_list = list(parcel_24_additional_sum_mapper.keys())
additional_mul_list = list(parcel_24_additional_mul_mapper.keys())
random_additional_list = choice(additional_sum_list + additional_mul_list)
if "cod" in random_additional_list:
    random_money_wire = choice(parcel_24_money_wire_mapper)
    random_ransom = round(uniform(parcel_24_min_ransom, parcel_24_max_ransom))


def test_parcel_24_success(app):
    params = {
        "delivery_location": random_location,
        "delivery_time": random_delivery_time,
        "deliver_from": random_city_from,
        "deliver_to": random_city_to,
        "denoted_value": random_denoted_value,
        "additional": random_additional_list,
        "ransom": random_ransom,
        "money_wire": random_money_wire,
    }
    if random_weight:
        params["weight"] = random_weight
    else:
        params["length"] = random_length
        params["width"] = random_width
        params["height"] = random_height

    response = app.get("/domestic/parcel-24", params=params)
    data, code = response.json(), response.status_code
    assert code == 200 and data.get("value") == calculate_parcel_24_cost(**params)


def test_parcel_24_fail(app):
    params = {"weight": weight_limit + 1}
    response = app.get("/domestic/parcel-24", params=params)
    data, code = response.json(), response.status_code

    assert code == 422
