from random import choice

from src.helpers.domestic.insured_mail import (
    calculate_insured_mail_cost,
    insured_mail_weight_mapper,
    insured_mail_value_mapper,
    insured_mail_additional_sum_mapper,
)


weight_list = list(insured_mail_weight_mapper.keys())
random_weight = choice(weight_list)
max_weight = weight_list[-1]
denoted_value_list = list(insured_mail_value_mapper.keys())
random_denoted_value = choice(denoted_value_list)
max_denoted_value = denoted_value_list[-1]
additional_sum_list = list(insured_mail_additional_sum_mapper.keys())
random_additional_sum = choice(additional_sum_list)


def test_calculate_insured_mail_success(app):
    params = {
        "weight": random_weight,
        "denoted_value": random_denoted_value,
        "additional": random_additional_sum,
    }
    response = app.get("/domestic/insured-mail", params=params)
    data, code = response.json(), response.status_code

    assert code == 200 and data.get("value") == calculate_insured_mail_cost(**params)


def test_calculate_insured_mail_fail(app):
    params = {"weight": max_weight + 1, "denoted_value": max_denoted_value + 1}
    response = app.get("/domestic/insured-mail", params=params)
    data, code = response.json(), response.status_code

    assert code == 422
