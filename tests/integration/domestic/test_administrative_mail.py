from random import choice

from src.helpers.domestic.administrative_mail import (
    administrative_mail_weight_mapper,
    administrative_mail_additional_sum_mapper,
    calculate_administrative_mail_cost,
)


weight_list = list(administrative_mail_weight_mapper.keys())
random_weight = choice(weight_list)
max_weight = weight_list[-1]
additional_sum_list = list(administrative_mail_additional_sum_mapper.keys())
random_additional_sum = choice(additional_sum_list)


def test_calculate_administrative_mail_success(app):
    params = {"weight": random_weight, "additional": random_additional_sum}
    response = app.get("/domestic/administrative-mail", params=params)
    data, code = response.json(), response.status_code

    assert code == 200 and data.get("value") == calculate_administrative_mail_cost(
        **params
    )


def test_calculate_administrative_mail_fail(app):
    params = {"weight": max_weight + 1}
    response = app.get("/domestic/administrative-mail", params=params)
    data, code = response.json(), response.status_code

    assert code == 422
