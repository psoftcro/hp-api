from random import choice

from src.helpers.domestic.priority_letter import (
    calculate_priority_letter_cost,
    priority_letter_weight_mapper,
)


weight_list = list(priority_letter_weight_mapper.keys())
random_weight = choice(weight_list)
max_weight = weight_list[-1]


def test_calculate_priority_letter_success(app):
    params = {"weight": random_weight}
    response = app.get("/domestic/priority-letter", params=params)
    data, code = response.json(), response.status_code

    assert code == 200 and data.get("value") == calculate_priority_letter_cost(**params)


def test_calculate_priority_letter_fail(app):
    params = {"weight": max_weight + 1}
    response = app.get("/domestic/priority-letter", params=params)
    data, code = response.json(), response.status_code

    assert code == 422
