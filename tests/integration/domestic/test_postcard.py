from random import choice

from src.helpers.domestic.postcard import calculate_postcard_cost, postcard_type_mapper


random_type = choice(list(postcard_type_mapper.keys()))


def test_calculate_postcard_success(app):
    params = {"postcard_type": random_type}
    response = app.get("/domestic/postcard", params=params)
    data, code = response.json(), response.status_code

    assert code == 200 and data.get("value") == calculate_postcard_cost(**params)


def test_calculate_postcard_fail(app):
    params = {"postcard_type": random_type + "_add"}
    response = app.get("/domestic/postcard", params=params)
    data, code = response.json(), response.status_code

    assert code == 422
