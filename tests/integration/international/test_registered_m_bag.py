from random import choice

from src.helpers.international.registered_m_bag import (
    calculate_registered_m_bag_cost,
    registered_m_bag_country_mapper,
    registered_m_bag_weight_mapper,
)


random_country = choice(list(registered_m_bag_country_mapper.keys()))
weight_list = list(registered_m_bag_weight_mapper.keys())
random_weight = choice(weight_list)
max_weight = weight_list[-1]


def test_calculate_registered_m_bag_success(app):
    params = {
        "country": random_country,
        "weight": random_weight,
    }
    response = app.get("/international/registered-m-bag", params=params)
    data, code = response.json(), response.status_code

    assert code == 200 and data.get("value") == calculate_registered_m_bag_cost(
        **params
    )


def test_calculate_registered_m_bag_fail(app):
    params = {"country": random_country + "_add", "weight": max_weight + 1}
    response = app.get("/international/registered-m-bag", params=params)
    data, code = response.json(), response.status_code

    assert code == 422
