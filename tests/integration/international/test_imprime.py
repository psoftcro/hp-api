from random import choice

from src.helpers.international.imprime import (
    calculate_imprime_cost,
    imprime_country_mapper,
    imprime_weight_mapper,
    imprime_additional_sum_mapper,
)


random_country = choice(list(imprime_country_mapper.keys()))
weight_list = list(imprime_weight_mapper.keys())
random_weight = choice(weight_list)
max_weight = weight_list[-1]
random_additional_list = choice(list(imprime_additional_sum_mapper.keys()))


def test_calculate_imprime_success(app):
    params = {
        "country": random_country,
        "weight": random_weight,
        "additional": random_additional_list,
    }
    response = app.get("/international/imprime", params=params)
    data, code = response.json(), response.status_code

    assert code == 200 and data.get("value") == calculate_imprime_cost(**params)


def test_calculate_imprime_fail(app):
    params = {"country": random_country + "_add", "weight": max_weight + 1}
    response = app.get("/international/imprime", params=params)
    data, code = response.json(), response.status_code

    assert code == 422
