from random import choice

from src.helpers.international.priority_letter import (
    calculate_priority_letter_cost,
    priority_letter_country_mapper,
    priority_letter_weight_mapper,
)


random_country = choice(list(priority_letter_country_mapper.keys()))
weight_list = list(priority_letter_weight_mapper.keys())
random_weight = choice(weight_list)
max_weight = weight_list[-1]


def test_calculate_priority_letter_success(app):
    params = {"country": random_country, "weight": random_weight}
    response = app.get("/international/priority-letter", params=params)
    data, code = response.json(), response.status_code

    assert code == 200 and data.get("value") == calculate_priority_letter_cost(**params)


def test_calculate_priority_letter_fail(app):
    params = {"country": random_country, "weight": max_weight + 1}
    response = app.get("/international/priority-letter", params=params)
    data, code = response.json(), response.status_code

    assert code == 422
