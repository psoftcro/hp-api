from random import choice, uniform

from src.helpers.international.parcel import (
    calculate_parcel_cost,
    parcel_country_mapper,
    parcel_weight_mapper,
)


random_country = choice(list(parcel_country_mapper.keys()))
random_weight = choice(list(parcel_weight_mapper.keys()))
random_parcel_type = choice(
    list(parcel_country_mapper.get(random_country).get("parcel_type").keys())
)
random_insured_value = None

if (
    parcel_country_mapper.get(random_country)
    .get("parcel_type")
    .get(random_parcel_type)
    .get("allowed_with_insured_value")
):
    random_insured_value = round(uniform(0, 9999))

random_additional_list = None

if (
    parcel_country_mapper.get(random_country)
    .get("parcel_type")
    .get(random_parcel_type)
    .get("additional")
):
    random_additional_list = choice(
        parcel_country_mapper.get(random_country)
        .get("parcel_type")
        .get(random_parcel_type)
        .get("additional")
    )
max_weight = list(parcel_weight_mapper.keys())[-1]


def test_calculate_parcel_success(app):
    params = {
        "country": random_country,
        "weight": random_weight,
        "parcel_type": random_parcel_type,
        "insured_value": random_insured_value,
        "additional": random_additional_list,
    }
    response = app.get("/international/parcel", params=params)
    data, code = response.json(), response.status_code

    assert code == 200 and data.get("value") == calculate_parcel_cost(**params)


def test_calculate_parcel_fail(app):
    params = {"country": random_country + "_add", "weight": max_weight + 1}
    response = app.get("/international/parcel", params=params)
    data, code = response.json(), response.status_code

    assert code == 422
