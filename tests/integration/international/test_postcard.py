from random import choice

from src.helpers.international.postcard import (
    calculate_postcard_cost,
    postcard_country_mapper,
)


random_country = choice(list(postcard_country_mapper.keys()))


def test_calculate_postcard_success(app):
    params = {"country": random_country}
    response = app.get("/international/postcard", params=params)
    data, code = response.json(), response.status_code

    assert code == 200 and data.get("value") == calculate_postcard_cost(**params)


def test_calculate_postcard_fail(app):
    params = {"country": random_country + "_add"}
    response = app.get("/international/postcard", params=params)
    data, code = response.json(), response.status_code

    assert code == 422
