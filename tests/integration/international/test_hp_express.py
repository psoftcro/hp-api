from random import choice

from src.helpers.international.hp_express import (
    calculate_hp_express_cost,
    hp_express_country_mapper,
    hp_express_weight_mapper,
    hp_express_delivery_locations,
)


random_country = choice(list(hp_express_country_mapper.keys()))
weight_list = list(key for key in hp_express_weight_mapper if key != "other")
random_weight = choice(weight_list)
max_weight = hp_express_country_mapper.get(random_country).get("max_weight")
random_delivery_location = choice(hp_express_delivery_locations)


def test_calculate_hp_express_success(app):
    params = {
        "country": random_country,
        "weight": random_weight,
        "delivery_location": random_delivery_location,
    }
    response = app.get("/international/hp-express", params=params)
    data, code = response.json(), response.status_code

    assert code == 200 and data.get("value") == calculate_hp_express_cost(**params)


def test_calculate_hp_express_fail(app):
    params = {"country": random_country + "_add", "weight": max_weight + 1}
    response = app.get("/international/hp-express", params=params)
    data, code = response.json(), response.status_code

    assert code == 422
