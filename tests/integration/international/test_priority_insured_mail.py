from random import choice, uniform

from src.helpers.international.priority_insured_mail import (
    calculate_priority_insured_mail_cost,
    priority_insured_mail_country_mapper,
    priority_insured_mail_weight_mapper,
    priority_insured_mail_additional_sum_mapper,
)


random_country = choice(list(priority_insured_mail_country_mapper.keys()))
weight_list = list(priority_insured_mail_weight_mapper.keys())
random_weight = choice(weight_list)
max_weight = weight_list[-1]
random_additional_list = choice(
    list(priority_insured_mail_additional_sum_mapper.keys())
)
max_denoted_value = priority_insured_mail_country_mapper.get(random_country).get(
    "max_denoted_value"
)
random_denoted_value = round(uniform(0, max_denoted_value))


def test_calculate_priority_insured_mail_success(app):
    params = {
        "country": random_country,
        "weight": random_weight,
        "denoted_value": random_denoted_value,
        "additional": random_additional_list,
    }
    response = app.get("/international/priority-insured-mail", params=params)
    data, code = response.json(), response.status_code

    assert code == 200 and data.get("value") == calculate_priority_insured_mail_cost(
        **params
    )


def test_calculate_priority_insured_mail_fail(app):
    params = {"country": random_country + "_add", "weight": max_weight + 1}
    response = app.get("/international/priority-insured-mail", params=params)
    data, code = response.json(), response.status_code

    assert code == 422
