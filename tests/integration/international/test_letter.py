from random import choice

from src.helpers.international.letter import (
    calculate_letter_cost,
    letter_country_mapper,
    letter_weight_mapper,
)


random_country = choice(list(letter_country_mapper.keys()))
weight_list = list(letter_weight_mapper.keys())
random_weight = choice(weight_list)
max_weight = weight_list[-1]


def test_calculate_letter_success(app):
    params = {"country": random_country, "weight": random_weight}
    response = app.get("/international/letter", params=params)
    data, code = response.json(), response.status_code

    assert code == 200 and data.get("value") == calculate_letter_cost(**params)


def test_calculate_letter_fail(app):
    params = {"country": random_country, "weight": max_weight + 1}
    response = app.get("/international/letter", params=params)
    data, code = response.json(), response.status_code

    assert code == 422
