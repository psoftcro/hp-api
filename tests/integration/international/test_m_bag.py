from random import choice

from src.helpers.international.m_bag import (
    calculate_m_bag_cost,
    m_bag_country_mapper,
    m_bag_weight_mapper,
    m_bag_additional_sum_mapper,
)


random_country = choice(list(m_bag_country_mapper.keys()))
weight_list = list(m_bag_weight_mapper.keys())
random_weight = choice(weight_list)
max_weight = weight_list[-1]
random_additional_list = choice(list(m_bag_additional_sum_mapper.keys()))


def test_calculate_m_bag_success(app):
    params = {
        "country": random_country,
        "weight": random_weight,
        "additional": random_additional_list,
    }
    response = app.get("/international/m-bag", params=params)
    data, code = response.json(), response.status_code

    assert code == 200 and data.get("value") == calculate_m_bag_cost(**params)


def test_calculate_m_bag_fail(app):
    params = {"country": random_country + "_add", "weight": max_weight + 1}
    response = app.get("/international/m-bag", params=params)
    data, code = response.json(), response.status_code

    assert code == 422
