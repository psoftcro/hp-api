from random import choice

from src.helpers.international.priority_registered_mail import (
    calculate_priority_registered_mail_cost,
    priority_registered_mail_country_mapper,
    priority_registered_mail_weight_mapper,
    priority_registered_mail_additional_sum_mapper,
)


random_country = choice(list(priority_registered_mail_country_mapper.keys()))
weight_list = list(priority_registered_mail_weight_mapper.keys())
random_weight = choice(weight_list)
max_weight = weight_list[-1]
random_additional_list = choice(
    list(priority_registered_mail_additional_sum_mapper.keys())
)


def test_calculate_priority_registered_mail_success(app):
    params = {
        "country": random_country,
        "weight": random_weight,
        "additional": random_additional_list,
    }
    response = app.get("/international/priority-registered-mail", params=params)
    data, code = response.json(), response.status_code

    assert code == 200 and data.get("value") == calculate_priority_registered_mail_cost(
        **params
    )


def test_calculate_priority_registered_mail_fail(app):
    params = {"country": random_country + "_add", "weight": max_weight + 1}
    response = app.get("/international/priority-registered-mail", params=params)
    data, code = response.json(), response.status_code

    assert code == 422
