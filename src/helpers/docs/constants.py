responses = {
    200: {
        "content": {
            "application/json": {
                "schema": {
                    "properties": {
                        "value": {
                            "description": "Value",
                            "example": 23,
                            "type": "number",
                        }
                    }
                }
            }
        },
        "description": "Success",
    },
    422: {
        "content": {
            "application/json": {
                "schema": {
                    "properties": {
                        "code": {
                            "description": "Error code",
                            "example": 422,
                            "type": "integer",
                        },
                        "errors": {
                            "description": "Error object",
                            "example": {"some_field": ["Required field"]},
                            "type": "object",
                        },
                        "message": {
                            "description": "Error message",
                            "example": "Validation Error",
                            "type": "string",
                        },
                    }
                }
            }
        },
        "description": "Validation Error",
    },
}
tags_metadata = [
    {
        "name": "Domestic",
        "description": "Domestic traffic shipment cost.",
        "externalDocs": {
            "description": "Find out more",
            "url": "https://www.posta.hr/en/mail-sending-rate-calculation/6617",
        },
    },
    {
        "name": "International",
        "description": "International traffic shipment cost.",
        "externalDocs": {
            "description": "Find out more",
            "url": "https://www.posta.hr/en/mail-sending-rate-calculation/6617",
        },
    },
    {
        "name": "Tracking",
        "description": "Get parcel tracking status.",
    },
]
