from src.helpers.currency import get_currency_schema, convert_value_to_currency
from src.helpers.international.postcard.mapper import postcard_country_mapper
from src.helpers.validator import Validator


def postcard_validator(*args, **kwargs):
    data = Validator.remove_empty_args(kwargs)
    postcard_countries = list(postcard_country_mapper.keys())
    schema = {
        "country": {
            "required": True,
            "nullable": False,
            "type": "string",
            "allowed": postcard_countries,
        },
        "currency": get_currency_schema(),
    }
    Validator.validate(data, schema)

    return Validator.c_errors if Validator.errors else None


def calculate_postcard_cost(*args, **kwargs):
    # country = kwargs["country"].lower()
    value = 8.6

    return (
        round(value, 2)
        if kwargs.get("currency") is None
        else round(convert_value_to_currency(value, kwargs["currency"]), 2)
    )
