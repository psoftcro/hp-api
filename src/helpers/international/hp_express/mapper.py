hp_express_country_mapper = {
    "AE": {"max_weight": 31500, "zone": "world"},
    "AG": {"max_weight": 20000, "zone": "world"},
    "AL": {"max_weight": 30000, "zone": "europe2"},
    "AM": {"max_weight": 20000, "zone": "europe2"},
    "AN": {"max_weight": 30000, "zone": "world"},
    "AR": {"max_weight": 20000, "zone": "america"},
    "AT": {"max_weight": 31500, "zone": "eu2"},
    "AU": {"max_weight": 20000, "zone": "world"},
    "AW": {"max_weight": 20000, "zone": "world"},
    "AZ": {"max_weight": 30000, "zone": "world"},
    "BA": {"max_weight": 30000, "zone": "europe1"},
    "BE": {"max_weight": 30000, "zone": "eu2"},
    "BG": {"max_weight": 30000, "zone": "eu2"},
    "BR": {"max_weight": 30000, "zone": "america"},
    "BT": {"max_weight": 30000, "zone": "world"},
    "BW": {"max_weight": 20000, "zone": "world"},
    "BY": {"max_weight": 20000, "zone": "europe2"},
    "CA": {"max_weight": 30000, "zone": "america"},
    "CH": {"max_weight": 30000, "zone": "europe2"},
    "CL": {"max_weight": 30000, "zone": "america"},
    "CN": {"max_weight": 30000, "zone": "world"},
    "CY": {"max_weight": 31500, "zone": "eu2"},
    "CZ": {"max_weight": 30000, "zone": "eu2"},
    "DE": {"max_weight": 30000, "zone": "eu2"},
    "DK": {"max_weight": 31500, "zone": "eu2"},
    "EC": {"max_weight": 30000, "zone": "america"},
    "EE": {"max_weight": 30000, "zone": "eu2"},
    "EG": {"max_weight": 20000, "zone": "world"},
    "ES": {"max_weight": 20000, "zone": "eu2"},
    "ET": {"max_weight": 30000, "zone": "world"},
    "FI": {"max_weight": 31500, "zone": "eu2"},
    "FR": {"max_weight": 30000, "zone": "eu2"},
    "GA": {"max_weight": 30000, "zone": "world"},
    "GH": {"max_weight": 30000, "zone": "world"},
    "GR": {"max_weight": 30000, "zone": "eu2"},
    "HK": {"max_weight": 30000, "zone": "world"},
    "HU": {"max_weight": 30000, "zone": "eu2"},
    "ID": {"max_weight": 30000, "zone": "world"},
    "IE": {"max_weight": 30000, "zone": "eu2"},
    "IL": {"max_weight": 20000, "zone": "world"},
    "IN": {"max_weight": 30000, "zone": "world"},
    "IR": {"max_weight": 20000, "zone": "world"},
    "IT": {"max_weight": 30000, "zone": "eu2"},
    "JP": {"max_weight": 30000, "zone": "world"},
    "KE": {"max_weight": 31500, "zone": "world"},
    "KN": {"max_weight": 30000, "zone": "world"},
    "KR": {"max_weight": 30000, "zone": "world"},
    "KW": {"max_weight": 30000, "zone": "world"},
    "KY": {"max_weight": 20000, "zone": "world"},
    "KZ": {"max_weight": 30000, "zone": "world"},
    "LA": {"max_weight": 30000, "zone": "world"},
    "LS": {"max_weight": 30000, "zone": "world"},
    "LT": {"max_weight": 31500, "zone": "eu2"},
    "LU": {"max_weight": 31000, "zone": "eu2"},
    "LV": {"max_weight": 30000, "zone": "eu2"},
    "LY": {"max_weight": 20000, "zone": "world"},
    "MA": {"max_weight": 30000, "zone": "world"},
    "MD": {"max_weight": 31500, "zone": "europe2"},
    "ME": {"max_weight": 30000, "zone": "europe1"},
    "MG": {"max_weight": 30000, "zone": "world"},
    "MK": {"max_weight": 30000, "zone": "europe2"},
    "MN": {"max_weight": 30000, "zone": "world"},
    "MT": {"max_weight": 30000, "zone": "eu2"},
    "MV": {"max_weight": 30000, "zone": "world"},
    "MX": {"max_weight": 20000, "zone": "america"},
    "MY": {"max_weight": 30000, "zone": "world"},
    "NL": {"max_weight": 30000, "zone": "eu2"},
    "NO": {"max_weight": 31500, "zone": "europe2"},
    "NZ": {"max_weight": 30000, "zone": "world"},
    "OM": {"max_weight": 30000, "zone": "world"},
    "PA": {"max_weight": 30000, "zone": "america"},
    "PE": {"max_weight": 31000, "zone": "america"},
    "PK": {"max_weight": 30000, "zone": "world"},
    "PL": {"max_weight": 20000, "zone": "eu2"},
    "PT": {"max_weight": 30000, "zone": "eu2"},
    "PY": {"max_weight": 25000, "zone": "america"},
    "QA": {"max_weight": 20000, "zone": "world"},
    "RO": {"max_weight": 31500, "zone": "eu2"},
    "RS": {"max_weight": 30000, "zone": "europe1"},
    "RU": {"max_weight": 30000, "zone": "eu2"},
    "RW": {"max_weight": 30000, "zone": "world"},
    "SA": {"max_weight": 30000, "zone": "world"},
    "SE": {"max_weight": 30000, "zone": "eu2"},
    "SG": {"max_weight": 30000, "zone": "world"},
    "SI": {"max_weight": 30000, "zone": "eu1"},
    "SK": {"max_weight": 30000, "zone": "eu2"},
    "SV": {"max_weight": 30000, "zone": "america"},
    "TD": {"max_weight": 30000, "zone": "world"},
    "TH": {"max_weight": 30000, "zone": "world"},
    "TR": {"max_weight": 30000, "zone": "europe2"},
    "TZ": {"max_weight": 30000, "zone": "world"},
    "UA": {"max_weight": 20000, "zone": "europe2"},
    "UK": {"max_weight": 30000, "zone": "eu2"},
    "US": {"max_weight": 31500, "zone": "america"},
    "UZ": {"max_weight": 30000, "zone": "world"},
    "VE": {"max_weight": 30000, "zone": "america"},
    "ZW": {"max_weight": 20000, "zone": "world"},
}

hp_express_weight_mapper = {
    200: {
        "eu1": 125,
        "eu2": 185,
        "europe1": 131,
        "europe2": 179,
        "america": 207.8,
        "world": 211,
    },
    500: {
        "eu1": 140,
        "eu2": 210,
        "europe1": 147,
        "europe2": 195,
        "america": 219,
        "world": 235,
    },
    1000: {
        "eu1": 162.5,
        "eu2": 247.5,
        "europe1": 171,
        "europe2": 203,
        "america": 247.8,
        "world": 251,
    },
    2000: {
        "eu1": 170,
        "eu2": 260,
        "europe1": 179,
        "europe2": 275,
        "america": 355,
        "world": 419,
    },
    5000: {
        "eu1": 200,
        "eu2": 310,
        "europe1": 211,
        "europe2": 315,
        "america": 419,
        "world": 515,
    },
    "other": {
        "eu1": 245,
        "eu2": 385,
        "europe1": 259,
        "europe2": 363,
        "america": 555,
        "world": 715,
    },
}
hp_express_multiplication_mapper = {
    "eu1": 5,
    "eu2": 7,
    "europe1": 4.8,
    "europe2": 6.4,
    "america": 12,
    "world": 16,
}
hp_express_delivery_locations = ["home_address", "post_office"]
