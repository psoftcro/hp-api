from src.helpers.currency import get_currency_schema, convert_value_to_currency
from src.helpers.international.hp_express.mapper import (
    hp_express_country_mapper,
    hp_express_weight_mapper,
    hp_express_multiplication_mapper,
    hp_express_delivery_locations,
)
from src.helpers.validator import Validator


def hp_express_validator(*args, **kwargs):
    data = Validator.remove_empty_args(kwargs)
    country_list = list(hp_express_country_mapper.keys())

    schema = {
        "country": {
            "required": True,
            "nullable": False,
            "numeric": False,
            "type": "string",
            "allowed": country_list,
        },
        "weight": {
            "numeric": True,
            "nullable": False,
            "coerce": "numeric",
            "required": True,
            "check_int_hp_express_max_weight": True,
            "min": 0.1,
        },
        "delivery_location": {
            "required": True,
            "nullable": False,
            "type": "string",
            "allowed": hp_express_delivery_locations,
        },
        "currency": get_currency_schema(),
    }
    Validator.data = hp_express_country_mapper
    Validator.validate(data, schema)
    Validator.data = None

    return Validator.c_errors if Validator.errors else None


def calculate_hp_express_cost(*args, **kwargs):
    country = kwargs["country"]
    weight = float(kwargs["weight"])
    value = None

    for key in hp_express_country_mapper:

        if key != country or not country:
            continue

        zone = hp_express_country_mapper.get(country).get("zone")

        for mapper_weight in hp_express_weight_mapper:

            if mapper_weight == "other" or weight > mapper_weight or weight == 0:
                continue

            value = hp_express_weight_mapper.get(mapper_weight).get(zone)
            break

        if not value:
            value = hp_express_weight_mapper.get("other").get(zone)

        if weight > 10000:
            multiplier = hp_express_multiplication_mapper.get(zone)
            calc = (weight - 10000) / 500
            mod = calc % 1

            if mod > 0:
                calc = calc - mod + 1

            value = value + (calc * multiplier)

        if kwargs["delivery_location"] == "post_office":
            value -= 35

        break

    return (
        round(value, 2)
        if kwargs.get("currency") is None
        else round(convert_value_to_currency(value, kwargs["currency"]), 2)
    )
