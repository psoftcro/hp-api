from src.helpers.currency import get_currency_schema, convert_value_to_currency
from src.helpers.international.parcel.mapper import (
    parcel_country_mapper,
    parcel_weight_mapper,
)
from src.helpers.validator import Validator


def parcel_validator(*args, **kwargs):
    data = Validator.remove_empty_args(kwargs)
    max_weight = list(parcel_weight_mapper.keys())[-1]
    country_list = list(parcel_country_mapper.keys())
    max_insured_value = list(
        parcel_weight_mapper.get(next(iter(parcel_weight_mapper)))
        .get("insured_value")
        .keys()
    )[-1]

    if kwargs.get("additional") is not None:
        data["additional"] = [
            param.strip().lower() for param in kwargs["additional"].split(",")
        ]

    schema = {
        "country": {
            "required": True,
            "nullable": False,
            "numeric": False,
            "type": "string",
            "allowed": country_list,
        },
        "weight": {
            "required": True,
            "nullable": False,
            "numeric": True,
            "coerce": "numeric",
            "max": max_weight,
            "min": 0.1,
        },
        "parcel_type": {
            "required": True,
            "nullable": False,
            "numeric": False,
            "type": "string",
            "check_int_parcel_type_additionals": True,
        },
        "insured_value": {
            "required": False,
            "nullable": True,
            "numeric": True,
            "check_int_parcel_insured_value": True,
            "max": max_insured_value,
            "min": 0,
        },
        "additional": {
            "required": False,
            "nullable": True,
            "type": "list",
            "check_int_parcel_type_additionals": True,
        },
        "currency": get_currency_schema(),
    }
    Validator.data = parcel_country_mapper
    Validator.validate(data, schema)
    Validator.data = None

    return Validator.c_errors if Validator.errors else None


def calculate_parcel_cost(*args, **kwargs):
    country = kwargs["country"]
    weight = float(kwargs["weight"])
    if kwargs.get("insured_value") is not None:
        insured_value = float(kwargs["insured_value"])
    else:
        insured_value = None
    value = None

    for key in parcel_country_mapper:

        if key != country or not country:
            continue

        zone = parcel_country_mapper.get(country).get("zone")

        for mapper_weight in parcel_weight_mapper:

            if weight > mapper_weight or weight == 0:
                continue

            value = (
                parcel_weight_mapper.get(mapper_weight)
                .get(kwargs["parcel_type"])
                .get(zone)
            )

            if kwargs.get("additional") is not None:
                additional = [
                    param.strip().lower() for param in kwargs["additional"].split(",")
                ]
            else:
                additional = []

            for param, num in (
                parcel_weight_mapper.get(mapper_weight).get("additional").items()
            ):

                if param not in additional or param not in parcel_country_mapper.get(
                    key
                ).get("parcel_type").get(kwargs["parcel_type"]).get("additional"):
                    continue

                value += num

            if "fragile" and "cumbersome" in additional:
                value -= (
                    parcel_weight_mapper.get(mapper_weight)
                    .get("additional")
                    .get("fragile")
                )

            if insured_value:
                for insured, num in (
                    parcel_weight_mapper.get(mapper_weight).get("insured_value").items()
                ):
                    if insured_value > insured or insured_value == 0:
                        continue
                    value += num
                    break

            break

        break

    return (
        round(value, 2)
        if kwargs.get("currency") is None
        else round(convert_value_to_currency(value, kwargs["currency"]), 2)
    )
