parcel_country_mapper = {
    "AC": {
        "parcel_type": {
            "economy": {
                "additional": ["fragile", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "AD": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 3475,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 2,
    },
    "AE": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "AF": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "AG": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "AI": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 10,
            },
        },
        "zone": 4,
    },
    "AL": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 2,
    },
    "AM": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 300,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 2,
    },
    "AN": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 7,
            },
            "premium": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "AO": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 10,
            },
        },
        "zone": 4,
    },
    "AR": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 1200,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 3,
    },
    "AS": {
        "parcel_type": {
            "economy": {
                "additional": ["cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "AT": {
        "parcel_type": {
            "premium": {
                "additional": ["ar", "fragile", "cod"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 1,
    },
    "AU": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 2500,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "AW": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 7,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "AZ": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 653,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "BA": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 4000,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 2,
    },
    "BB": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "BD": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 10,
            },
        },
        "zone": 4,
    },
    "BE": {
        "parcel_type": {
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 1,
    },
    "BF": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 544,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "BG": {
        "parcel_type": {
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 6,
    },
    "BH": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "BI": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 585,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "BJ": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "BM": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "BN": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 1000,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 10,
            },
        },
        "zone": 4,
    },
    "BO": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 3,
    },
    "BR": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 4000,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 3,
    },
    "BS": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "BT": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 10,
            },
        },
        "zone": 4,
    },
    "BW": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 156,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "BY": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 900,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 2,
    },
    "BZ": {
        "parcel_type": {
            "economy": {
                "additional": ["fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 1187,
            },
            "premium": {
                "additional": ["fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 20,
            },
        },
        "zone": 3,
    },
    "CA": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "CC": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 1380,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "CD": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "CH": {
        "parcel_type": {
            "premium": {
                "additional": ["cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 7,
    },
    "CK": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 400,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 10,
            },
        },
        "zone": 4,
    },
    "CL": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 3,
    },
    "CM": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 1000,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "CN": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 838,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "CO": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 3,
    },
    "CR": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "CU": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "CV": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 326,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "CX": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "CY": {
        "parcel_type": {
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 6,
    },
    "CZ": {
        "parcel_type": {
            "premium": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 5,
    },
    "DE": {
        "parcel_type": {
            "premium": {
                "additional": ["ar", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 1,
    },
    "DJ": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "DK": {
        "parcel_type": {
            "premium": {
                "additional": ["ar", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            }
        },
        "zone": 1,
    },
    "DM": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "DO": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "DZ": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 80,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "EC": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 3,
    },
    "EE": {
        "parcel_type": {
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 6,
    },
    "EG": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 1250,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "ER": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "ES": {
        "parcel_type": {
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 5,
    },
    "ET": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "FI": {
        "parcel_type": {
            "premium": {
                "additional": ["ar", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 5,
    },
    "FJ": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "FK": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "FM": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": False,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": False,
                "weight_limit": 0,
            },
        },
        "zone": 4,
    },
    "FO": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 4000,
            },
            "premium": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 2,
    },
    "FR": {
        "parcel_type": {
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 5,
    },
    "GA": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "GD": {
        "parcel_type": {
            "economy": {
                "additional": ["fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "GE": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 1000,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "GF": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 3475,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 3,
    },
    "GG": {
        "parcel_type": {
            "economy": {
                "additional": ["fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 2,
    },
    "GH": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "GI": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "cumbersome", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "cumbersome", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 2,
    },
    "GL": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 4000,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 2,
    },
    "GM": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "GN": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "GP": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "GQ": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "GR": {
        "parcel_type": {
            "premium": {
                "additional": ["ar", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            }
        },
        "zone": 6,
    },
    "GS": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "GT": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 3,
    },
    "GU": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "GW": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "GY": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 8,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "HK": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 4000,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "HN": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 3,
    },
    "HT": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "HU": {
        "parcel_type": {
            "premium": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 5,
    },
    "IC": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "ID": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "IE": {
        "parcel_type": {
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 6,
    },
    "IL": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "IM": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 2,
    },
    "IN": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 1502,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "IQ": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "IR": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 10,
            },
        },
        "zone": 4,
    },
    "IS": {
        "parcel_type": {
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 7,
    },
    "IT": {
        "parcel_type": {
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 5,
    },
    "JM": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 15,
            },
        },
        "zone": 4,
    },
    "JO": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "JP": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 4000,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "KE": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "KG": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 1000,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "KH": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "KI": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "KM": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 512,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "KN": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 154,
            },
            "premium": {
                "additional": ["ar", "", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "KP": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 3266,
            },
            "premium": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "KR": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 4000,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "KW": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "KY": {
        "parcel_type": {
            "economy": {
                "additional": ["fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "KZ": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 4000,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "LA": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "LB": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 10,
            },
        },
        "zone": 4,
    },
    "LC": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": False,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "LI": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 4000,
            },
            "premium": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 2,
    },
    "LK": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "LR": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "LS": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 40,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "LT": {
        "parcel_type": {
            "premium": {
                "additional": ["ar", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 6,
    },
    "LU": {
        "parcel_type": {
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 5,
    },
    "LV": {
        "parcel_type": {
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 6,
    },
    "LY": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "MA": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 3475,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "MC": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 3475,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 2,
    },
    "MD": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 2000,
            },
            "premium": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 2,
    },
    "ME": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 4000,
            },
            "premium": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 2,
    },
    "MG": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "MH": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": False,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": False,
                "weight_limit": 0,
            },
        },
        "zone": 4,
    },
    "MK": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 1633,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 2,
    },
    "ML": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "MM": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "MN": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "MO": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 2900,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "MP": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "MQ": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "MR": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "MS": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "MT": {
        "parcel_type": {
            "premium": {
                "additional": ["fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            }
        },
        "zone": 6,
    },
    "MU": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 160,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "MV": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "MW": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 37,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "MX": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 3,
    },
    "MY": {
        "parcel_type": {
            "economy": {
                "additional": ["fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 980,
            },
            "premium": {
                "additional": ["fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "MZ": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "NA": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 1278,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "NC": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 1175,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "NE": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "NF": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 1380,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "NG": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "NI": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 3,
    },
    "NL": {
        "parcel_type": {
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 1,
    },
    "NO": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 2,
    },
    "NP": {
        "parcel_type": {
            "economy": {
                "additional": ["fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "NR": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 50,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "NU": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 10,
            },
        },
        "zone": 4,
    },
    "NZ": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 800,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "OM": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "PA": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 3,
    },
    "PE": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 3,
    },
    "PF": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 753,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "PG": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 100,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "PH": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 2,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "PK": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 900,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "PL": {
        "parcel_type": {
            "premium": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            }
        },
        "zone": 5,
    },
    "PM": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "PN": {
        "parcel_type": {
            "economy": {
                "additional": ["fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 10,
            },
        },
        "zone": 4,
    },
    "PR": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "PT": {
        "parcel_type": {
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 5,
    },
    "PW": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": False,
                "weight_limit": 0,
            },
        },
        "zone": 4,
    },
    "PY": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 3,
    },
    "QA": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 653,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "RE": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "RO": {
        "parcel_type": {
            "premium": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 5,
    },
    "RS": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 4000,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 2,
    },
    "RU": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 4000,
            },
            "premium": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 2,
    },
    "RW": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "SA": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "SB": {
        "parcel_type": {
            "economy": {
                "additional": ["fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "SC": {
        "parcel_type": {
            "economy": {
                "additional": ["fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "SE": {
        "parcel_type": {
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            }
        },
        "zone": 5,
    },
    "SG": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 2000,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "SH": {
        "parcel_type": {
            "economy": {
                "additional": ["fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 126,
            },
            "premium": {
                "additional": ["fragile", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "SI": {
        "parcel_type": {
            "premium": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 1,
    },
    "SK": {
        "parcel_type": {
            "premium": {
                "additional": ["fragile", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            }
        },
        "zone": 5,
    },
    "SL": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "SM": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 1633,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 2,
    },
    "SN": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 562,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "SO": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": False,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": False,
                "weight_limit": 20,
            },
        },
        "zone": 0,  # Should be in range (1-7)
    },
    "SR": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 3,
    },
    "ST": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "SU": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "SV": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 3,
    },
    "SY": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 653,
            },
            "premium": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "SZ": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 1,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "TA": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 10,
            },
        },
        "zone": 4,
    },
    "TC": {
        "parcel_type": {
            "economy": {
                "additional": ["fragile", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["fragile", "cumbersome"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 10,
            },
        },
        "zone": 4,
    },
    "TD": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 326,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "TG": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "TH": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 1000,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "TJ": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 947,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "TK": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "TL": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": False,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "TM": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 500,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "TN": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 1633,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "TO": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 383,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "TR": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 653,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 2,
    },
    "TT": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 10,
            },
        },
        "zone": 4,
    },
    "TV": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 2200,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "TW": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 652,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "TZ": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "UA": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 4000,
            },
            "premium": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 2,
    },
    "UG": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "UK": {
        "parcel_type": {
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            }
        },
        "zone": 1,
    },
    "US": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 3448,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 30,
            },
        },
        "zone": 3,
    },
    "UY": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": False,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 3,
    },
    "UZ": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 4000,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "VA": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 1633,
            },
            "premium": {
                "additional": ["ar", "fragile", "cumbersome"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 2,
    },
    "VC": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 98,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 10,
            },
        },
        "zone": 4,
    },
    "VE": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 3,
    },
    "VG": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "VT": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "VU": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "WF": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "WS": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 220,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "XK": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 2,
    },
    "YE": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 0,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "YT": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": False,
                "weight_limit": 0,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": False,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "ZA": {
        "parcel_type": {
            "economy": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 1307,
            },
            "premium": {
                "additional": [],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": True,
                "weight_limit": 20,
            },
        },
        "zone": 4,
    },
    "ZM": {
        "parcel_type": {
            "economy": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 400,
            },
            "premium": {
                "additional": ["ar"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
    "ZW": {
        "parcel_type": {
            "economy": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 980,
            },
            "premium": {
                "additional": ["ar", "fragile"],
                "allowed_with_insured_value": True,
                "allowed_without_insured_value": False,
                "weight_limit": 30,
            },
        },
        "zone": 4,
    },
}

parcel_weight_mapper = {
    1: {
        "economy": {1: 75, 2: 120, 3: 150, 4: 180, 5: 90, 6: 144, 7: 100},
        "premium": {1: 75, 2: 144, 3: 174, 4: 190, 5: 90, 6: 144, 7: 100},
        "additional": {"ar": 6.4, "cod": 6, "fragile": 35, "cumbersome": 35},
        "insured_value": {1300: 0, 3000: 44, 5000: 76, 7000: 104, 10000: 148},
    },
    2: {
        "economy": {1: 90, 2: 140, 3: 180, 4: 220, 5: 120, 6: 188, 7: 130},
        "premium": {1: 90, 2: 188, 3: 236, 4: 300, 5: 120, 6: 188, 7: 130},
        "additional": {"ar": 6.4, "cod": 6, "fragile": 35, "cumbersome": 35},
        "insured_value": {1300: 0, 3000: 44, 5000: 76, 7000: 104, 10000: 148},
    },
    5: {
        "economy": {1: 110, 2: 220, 3: 250, 4: 320, 5: 165, 6: 260, 7: 180},
        "premium": {1: 110, 2: 260, 3: 306, 4: 400, 5: 165, 6: 260, 7: 180},
        "additional": {"ar": 6.4, "cod": 6, "fragile": 35, "cumbersome": 35},
        "insured_value": {1300: 0, 3000: 44, 5000: 76, 7000: 104, 10000: 148},
    },
    10: {
        "economy": {1: 150, 2: 270, 3: 400, 4: 500, 5: 230, 6: 310, 7: 250},
        "premium": {1: 150, 2: 310, 3: 480, 4: 580, 5: 230, 6: 310, 7: 250},
        "additional": {"ar": 6.4, "cod": 6, "fragile": 35, "cumbersome": 35},
        "insured_value": {1300: 0, 3000: 44, 5000: 76, 7000: 104, 10000: 148},
    },
    15: {
        "economy": {1: 240, 2: 280, 3: 440, 4: 520, 5: 350, 6: 400, 7: 320},
        "premium": {1: 240, 2: 320, 3: 520, 4: 600, 5: 350, 6: 400, 7: 320},
        "additional": {"ar": 8.6, "cod": 20, "fragile": 35, "cumbersome": 35},
        "insured_value": {1300: 0, 3000: 55, 5000: 95, 7000: 130, 10000: 185},
    },
    20: {
        "economy": {1: 280, 2: 320, 3: 520, 4: 640, 5: 415, 6: 450, 7: 360},
        "premium": {1: 280, 2: 360, 3: 600, 4: 680, 5: 415, 6: 450, 7: 360},
        "additional": {"ar": 8.6, "cod": 20, "fragile": 35, "cumbersome": 35},
        "insured_value": {1300: 0, 3000: 55, 5000: 95, 7000: 130, 10000: 185},
    },
    25: {
        "economy": {1: 320, 2: 400, 3: 600, 4: 720, 5: 500, 6: 550, 7: 440},
        "premium": {1: 320, 2: 440, 3: 680, 4: 760, 5: 500, 6: 550, 7: 440},
        "additional": {"ar": 8.6, "cod": 20, "fragile": 35, "cumbersome": 35},
        "insured_value": {1300: 0, 3000: 55, 5000: 95, 7000: 130, 10000: 185},
    },
    30: {
        "economy": {1: 360, 2: 480, 3: 680, 4: 800, 5: 610, 6: 650, 7: 520},
        "premium": {1: 360, 2: 520, 3: 760, 4: 880, 5: 610, 6: 650, 7: 520},
        "additional": {"ar": 8.6, "cod": 20, "fragile": 35, "cumbersome": 35},
        "insured_value": {1300: 0, 3000: 55, 5000: 95, 7000: 130, 10000: 185},
    },
}
