from src.helpers.currency import get_currency_schema, convert_value_to_currency
from src.helpers.international.priority_registered_mail.mapper import (
    priority_registered_mail_country_mapper,
    priority_registered_mail_weight_mapper,
    priority_registered_mail_additional_sum_mapper,
)
from src.helpers.validator import Validator


def priority_registered_mail_validator(*args, **kwargs):
    data = Validator.remove_empty_args(kwargs)
    priority_registered_mail_countries = list(
        priority_registered_mail_country_mapper.keys()
    )
    max_weight = list(priority_registered_mail_weight_mapper.keys())[-1]
    additional_priority_registered_mail_params = list(
        priority_registered_mail_additional_sum_mapper.keys()
    )

    if kwargs.get("additional") is not None:
        data["additional"] = [
            param.strip().lower() for param in kwargs["additional"].split(",")
        ]

    schema = {
        "country": {
            "required": True,
            "nullable": False,
            "type": "string",
            "allowed": priority_registered_mail_countries,
        },
        "weight": {
            "required": True,
            "nullable": False,
            "numeric": True,
            "coerce": "numeric",
            "max": max_weight,
            "min": 0.1,
        },
        "additional": {
            "required": False,
            "nullable": True,
            "type": "list",
            "allowed": additional_priority_registered_mail_params,
        },
        "currency": get_currency_schema(),
    }
    Validator.validate(data, schema)

    return Validator.c_errors if Validator.errors else None


def calculate_priority_registered_mail_cost(*args, **kwargs):
    # country = kwargs["country"].lower()
    weight = float(kwargs["weight"])
    value = None

    for key in priority_registered_mail_weight_mapper:

        if weight > key or weight == 0:
            continue

        value = priority_registered_mail_weight_mapper.get(key)
        break

    if kwargs.get("additional") is not None:
        additional = [
            param.strip().lower() for param in kwargs["additional"].split(",")
        ]
    else:
        additional = []

    for param, num in priority_registered_mail_additional_sum_mapper.items():

        if param not in additional:
            continue

        value += num

    return (
        round(value, 2)
        if kwargs.get("currency") is None
        else round(convert_value_to_currency(value, kwargs["currency"]), 2)
    )
