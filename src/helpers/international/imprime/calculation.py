from src.helpers.currency import get_currency_schema, convert_value_to_currency
from src.helpers.international.imprime.mapper import (
    imprime_country_mapper,
    imprime_weight_mapper,
    imprime_additional_sum_mapper,
)
from src.helpers.validator import Validator


def imprime_validator(*args, **kwargs):
    data = Validator.remove_empty_args(kwargs)
    imprime_countries = list(imprime_country_mapper.keys())
    max_weight = list(imprime_weight_mapper.keys())[-1]
    additional_imprime_params = list(imprime_additional_sum_mapper.keys())

    if kwargs.get("additional") is not None:
        data["additional"] = [
            param.strip().lower() for param in kwargs["additional"].split(",")
        ]

    schema = {
        "country": {
            "required": True,
            "nullable": False,
            "type": "string",
            "allowed": imprime_countries,
        },
        "weight": {
            "required": True,
            "nullable": False,
            "numeric": True,
            "coerce": "numeric",
            "max": max_weight,
            "min": 0.1,
        },
        "additional": {
            "required": False,
            "nullable": True,
            "type": "list",
            "allowed": additional_imprime_params,
        },
        "currency": get_currency_schema(),
    }
    Validator.validate(data, schema)

    return Validator.c_errors if Validator.errors else None


def calculate_imprime_cost(*args, **kwargs):
    # country = kwargs["country"].lower()
    weight = float(kwargs["weight"])
    key = None
    value = None

    for key in imprime_weight_mapper:

        if weight > key or weight == 0:
            continue

        value = imprime_weight_mapper.get(key)
        break

    if kwargs.get("additional") is not None:
        additional = [
            param.strip().lower() for param in kwargs["additional"].split(",")
        ]
    else:
        additional = []

    for param in imprime_additional_sum_mapper:

        if param not in additional:
            continue

        value += imprime_additional_sum_mapper.get(param).get(key)

    return (
        round(value, 2)
        if kwargs.get("currency") is None
        else round(convert_value_to_currency(value, kwargs["currency"]), 2)
    )
