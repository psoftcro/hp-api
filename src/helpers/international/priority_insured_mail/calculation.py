from src.helpers.currency import get_currency_schema, convert_value_to_currency
from src.helpers.international.priority_insured_mail.mapper import (
    priority_insured_mail_country_mapper,
    priority_insured_mail_weight_mapper,
    priority_insured_mail_additional_sum_mapper,
)
from src.helpers.validator import Validator


def priority_insured_mail_validator(*args, **kwargs):
    data = Validator.remove_empty_args(kwargs)
    priority_insured_mail_countries = list(priority_insured_mail_country_mapper.keys())
    max_weight = list(priority_insured_mail_weight_mapper.keys())[-1]

    if kwargs.get("additional") is not None:
        data["additional"] = [
            param.strip().lower() for param in kwargs["additional"].split(",")
        ]

    schema = {
        "country": {
            "required": True,
            "nullable": False,
            "type": "string",
            "allowed": priority_insured_mail_countries,
        },
        "weight": {
            "required": True,
            "nullable": False,
            "numeric": True,
            "coerce": "numeric",
            "max": max_weight,
            "min": 0.1,
        },
        "denoted_value": {
            "required": False,
            "nullable": True,
            "numeric": True,
            "check_int_priority_insured_mail_denoted_value": True,
            "min": 0,
        },
        "additional": {
            "required": False,
            "nullable": True,
            "type": "list",
            "check_int_priority_insured_mail_additionals": True,
        },
        "currency": get_currency_schema(),
    }
    Validator.data = priority_insured_mail_country_mapper
    Validator.validate(data, schema)
    Validator.data = None

    return Validator.c_errors if Validator.errors else None


def calculate_priority_insured_mail_cost(*args, **kwargs):
    country = kwargs["country"]
    weight = float(kwargs["weight"])
    denoted_value = float(kwargs["denoted_value"])
    dts = 601.744
    value = None

    for key in priority_insured_mail_country_mapper:

        if key != country or not country:
            continue

        # zone = priority_insured_mail_country_mapper.get(country).get("zone")

        for mapper_weight in priority_insured_mail_weight_mapper:

            if weight > mapper_weight or weight == 0:
                continue

            value = priority_insured_mail_weight_mapper.get(mapper_weight)

            if kwargs.get("additional") is not None:
                additional = [
                    param.strip().lower() for param in kwargs["additional"].split(",")
                ]
            else:
                additional = []

            for param, num in priority_insured_mail_additional_sum_mapper.items():

                if param not in additional:
                    continue

                value += num

            mod_value = denoted_value % dts

            if mod_value > 0:
                denoted_value = denoted_value - mod_value + dts

            value = value + denoted_value * 0.1080 / 65 * 4

            break

        break

    return (
        round(value, 2)
        if kwargs.get("currency") is None
        else round(convert_value_to_currency(value, kwargs["currency"]), 2)
    )
