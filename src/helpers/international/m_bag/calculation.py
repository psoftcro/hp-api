from src.helpers.currency import get_currency_schema, convert_value_to_currency
from src.helpers.international.m_bag.mapper import (
    m_bag_country_mapper,
    m_bag_weight_mapper,
    m_bag_additional_sum_mapper,
)
from src.helpers.validator import Validator


def m_bag_validator(*args, **kwargs):
    data = Validator.remove_empty_args(kwargs)
    m_bag_countries = list(m_bag_country_mapper.keys())
    max_weight = list(m_bag_weight_mapper.keys())[-1]
    additional_m_bag_params = list(m_bag_additional_sum_mapper.keys())

    if kwargs.get("additional") is not None:
        data["additional"] = [
            param.strip().lower() for param in kwargs["additional"].split(",")
        ]

    schema = {
        "country": {
            "required": True,
            "nullable": False,
            "type": "string",
            "allowed": m_bag_countries,
        },
        "weight": {
            "required": True,
            "nullable": False,
            "numeric": True,
            "coerce": "numeric",
            "max": max_weight,
            "min": 0.1,
        },
        "additional": {
            "required": False,
            "nullable": True,
            "type": "list",
            "allowed": additional_m_bag_params,
        },
        "currency": get_currency_schema(),
    }
    Validator.validate(data, schema)

    return Validator.c_errors if Validator.errors else None


def calculate_m_bag_cost(*args, **kwargs):
    country = kwargs["country"]
    weight = float(kwargs["weight"])
    value = None

    for key in m_bag_country_mapper:

        if key != country or not country:
            continue

        zone = m_bag_country_mapper.get(country)

        for mapper_weight in m_bag_weight_mapper:

            if weight > mapper_weight or weight == 0:
                continue

            value = m_bag_weight_mapper.get(mapper_weight).get("zone").get(zone)

            if kwargs.get("additional") is not None:
                additional = [
                    param.strip().lower() for param in kwargs["additional"].split(",")
                ]
            else:
                additional = []

            for param in m_bag_additional_sum_mapper:

                if param not in additional:
                    continue

                value += (
                    m_bag_additional_sum_mapper.get(param)
                    .get(mapper_weight)
                    .get("zone")
                    .get(zone)
                )

            break

    return (
        round(value, 2)
        if kwargs.get("currency") is None
        else round(convert_value_to_currency(value, kwargs["currency"]), 2)
    )
