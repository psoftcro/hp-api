from src.helpers.currency import get_currency_schema, convert_value_to_currency
from src.helpers.international.registered_m_bag.mapper import (
    registered_m_bag_country_mapper,
    registered_m_bag_weight_mapper,
)
from src.helpers.validator import Validator


def registered_m_bag_validator(*args, **kwargs):
    data = Validator.remove_empty_args(kwargs)
    registered_m_bag_countries = list(registered_m_bag_country_mapper.keys())
    max_weight = list(registered_m_bag_weight_mapper.keys())[-1]

    schema = {
        "country": {
            "required": True,
            "nullable": False,
            "type": "string",
            "allowed": registered_m_bag_countries,
        },
        "weight": {
            "required": True,
            "nullable": False,
            "numeric": True,
            "coerce": "numeric",
            "max": max_weight,
            "min": 0.1,
        },
        "currency": get_currency_schema(),
    }
    Validator.validate(data, schema)

    return Validator.c_errors if Validator.errors else None


def calculate_registered_m_bag_cost(*args, **kwargs):
    country = kwargs["country"]
    weight = float(kwargs["weight"])
    value = None

    for key in registered_m_bag_country_mapper:

        if key != country or not country:
            continue

        zone = registered_m_bag_country_mapper.get(country)

        for mapper_weight in registered_m_bag_weight_mapper:

            if weight > mapper_weight or weight == 0:
                continue

            value = (
                registered_m_bag_weight_mapper.get(mapper_weight).get("zone").get(zone)
            )

            break

    return (
        round(value, 2)
        if kwargs.get("currency") is None
        else round(convert_value_to_currency(value, kwargs["currency"]), 2)
    )
