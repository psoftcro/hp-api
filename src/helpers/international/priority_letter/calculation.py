from src.helpers.currency import get_currency_schema, convert_value_to_currency
from src.helpers.international.priority_letter.mapper import (
    priority_letter_country_mapper,
    priority_letter_weight_mapper,
)
from src.helpers.validator import Validator


def priority_letter_validator(*args, **kwargs):
    data = Validator.remove_empty_args(kwargs)
    priority_letter_countries = list(priority_letter_country_mapper.keys())
    max_weight = list(priority_letter_weight_mapper.keys())[-1]
    schema = {
        "country": {
            "required": True,
            "nullable": False,
            "type": "string",
            "allowed": priority_letter_countries,
        },
        "weight": {
            "required": True,
            "nullable": False,
            "numeric": True,
            "coerce": "numeric",
            "max": max_weight,
            "min": 0.1,
        },
        "currency": get_currency_schema(),
    }
    Validator.validate(data, schema)

    return Validator.c_errors if Validator.errors else None


def calculate_priority_letter_cost(*args, **kwargs):
    # country = kwargs["country"].lower()
    weight = float(kwargs["weight"])

    for key in priority_letter_weight_mapper:

        if weight > key or weight == 0:
            continue

        value = priority_letter_weight_mapper.get(key)

        return (
            round(value, 2)
            if kwargs.get("currency") is None
            else round(convert_value_to_currency(value, kwargs["currency"]), 2)
        )
