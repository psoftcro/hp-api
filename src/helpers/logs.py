import logging

import gunicorn.glogging as glog


class HPApiGunicornLogger(glog.Logger):
    def setup(self, cfg):
        self.access_log = logging.getLogger("uvicorn.access")
        super().setup(cfg)
