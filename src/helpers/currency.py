import json
from hashlib import md5

import requests

from src.helpers.redis import get_redis

redis_client = get_redis()


def _get_currency_info():
    currency_info = redis_client.get("currency_course")

    if not currency_info:
        refresh_course_list()
        currency_info = redis_client.get("currency_course")

    return currency_info


def _get_currencies_list():
    return list(json.loads(_get_currency_info()).keys())


def get_currency_schema():
    return {
        "required": False,
        "nullable": True,
        "type": "string",
        "allowed": _get_currencies_list(),
    }


def refresh_course_list():
    try:
        response = requests.get("http://api.hnb.hr/tecajn/v2")
    except Exception:
        return
    currency_list = json.loads(response.content)
    currency_info = {}

    for currency_element in currency_list:
        currency_info[currency_element["valuta"]] = currency_element

    old_hash = redis_client.get("currency_hash")
    old_currency = redis_client.get("currency_course")
    new_hash = md5(
        json.dumps(currency_info, sort_keys=True).encode("utf-8")
    ).hexdigest()

    if not old_hash or not old_currency or old_hash.decode("utf-8") != new_hash:
        redis_client.set("currency_hash", new_hash)
        redis_client.set("currency_course", json.dumps(currency_info))


def convert_value_to_currency(value, currency):
    find_course = json.loads(_get_currency_info()).get(currency.upper())
    target_course = float(find_course.get("srednji_tecaj").replace(",", "."))

    return round(value / target_course, 2)
