administrative_mail_weight_mapper = {
    50: 19.10,
    100: 20.60,
    250: 22.60,
    500: 26.10,
    1000: 30.10,
    2000: 36.60,
}
administrative_mail_additional_sum_mapper = {"ptr": 8}
