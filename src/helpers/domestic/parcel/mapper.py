parcel_weight_mapper = {
    2000: {
        "delivery_location": {"post_office": 23, "home_address": 25},
        "multiplier": 1,
    },
    5000: {
        "delivery_location": {"post_office": 25, "home_address": 30},
        "multiplier": 1,
    },
    10000: {
        "delivery_location": {"post_office": 28, "home_address": 33},
        "multiplier": 1,
    },
    15000: {
        "delivery_location": {"post_office": 35, "home_address": 40},
        "multiplier": 1.25,
    },
    20000: {
        "delivery_location": {"post_office": 38, "home_address": 43},
        "multiplier": 1.25,
    },
}
parcel_additional_mul_mapper = {"cod": 6, "prr": 3.1, "sbp": 12, "ptr": 8}
parcel_additional_sum_mapper = {"sd": 12.5, "narop": 25}
parcel_min_denoted_value = 3
parcel_max_denoted_value = 100000
