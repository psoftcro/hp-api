from src.helpers.currency import get_currency_schema, convert_value_to_currency
from src.helpers.domestic.parcel.mapper import (
    parcel_weight_mapper,
    parcel_additional_sum_mapper,
    parcel_additional_mul_mapper,
    parcel_min_denoted_value,
    parcel_max_denoted_value,
)
from src.helpers.validator import Validator


def parcel_validator(*args, **kwargs):
    data = Validator.remove_empty_args(kwargs)
    max_weight = list(parcel_weight_mapper.keys())[-1]
    additional_parcel_params = list(parcel_additional_sum_mapper.keys()) + list(
        parcel_additional_mul_mapper.keys()
    )
    delivery_locations = (
        parcel_weight_mapper.get(next(iter(parcel_weight_mapper)))
        .get("delivery_location")
        .keys()
    )

    if kwargs.get("additional") is not None:
        data["additional"] = [
            param.strip().lower() for param in kwargs["additional"].split(",")
        ]

    schema = {
        "weight": {
            "required": True,
            "nullable": False,
            "numeric": True,
            "coerce": "numeric",
            "max": max_weight,
            "min": 0.1,
        },
        "delivery_location": {
            "required": True,
            "nullable": False,
            "type": "string",
            "allowed": delivery_locations,
        },
        "denoted_value": {
            "required": True,
            "nullable": False,
            "numeric": True,
            "coerce": "numeric",
            "min": parcel_min_denoted_value,
            "max": parcel_max_denoted_value,
        },
        "additional": {
            "required": False,
            "nullable": True,
            "type": "list",
            "allowed": additional_parcel_params,
        },
        "currency": get_currency_schema(),
    }
    Validator.validate(data, schema)

    return Validator.c_errors if Validator.errors else None


def calculate_parcel_cost(*args, **kwargs):
    weight = float(kwargs["weight"])
    denoted_value = float(kwargs["denoted_value"])

    multiplier = None
    value = None

    for key in parcel_weight_mapper:

        if weight >= key or weight == 0:
            continue

        value = (
            parcel_weight_mapper.get(key)
            .get("delivery_location")
            .get(kwargs["delivery_location"])
        )
        multiplier = parcel_weight_mapper.get(key).get("multiplier")
        break

    if not value:
        return None

    if kwargs.get("additional") is not None:
        additional = [
            param.strip().lower() for param in kwargs["additional"].split(",")
        ]
    else:
        additional = []

    for param, num in parcel_additional_sum_mapper.items():

        if param not in additional:
            continue

        value += num

    for param, num in parcel_additional_mul_mapper.items():

        if param not in additional:
            continue

        value = value + (num * multiplier)

    if denoted_value > 100:
        denoted_calc = denoted_value * parcel_min_denoted_value / 100

        if denoted_calc < parcel_min_denoted_value:
            denoted_calc = 3
        elif denoted_calc > 220:
            denoted_calc = 220
        else:
            denoted_calc = denoted_calc

        value += denoted_calc

    return (
        round(value, 2)
        if kwargs.get("currency") is None
        else round(convert_value_to_currency(value, kwargs["currency"]), 2)
    )
