from src.helpers.currency import get_currency_schema, convert_value_to_currency
from src.helpers.domestic.letter.mapper import letter_weight_mapper
from src.helpers.validator import Validator


def letter_validator(*args, **kwargs):
    data = Validator.remove_empty_args(kwargs)
    max_weight = list(letter_weight_mapper.keys())[-1]
    schema = {
        "weight": {
            "required": True,
            "nullable": False,
            "numeric": True,
            "coerce": "numeric",
            "max": max_weight,
            "min": 0.1,
        },
        "currency": get_currency_schema(),
    }
    Validator.validate(data, schema)

    return Validator.c_errors if Validator.errors else None


def calculate_letter_cost(*args, **kwargs):
    weight = float(kwargs["weight"])

    for key in letter_weight_mapper:

        if weight > key or weight == 0:
            continue

        value = letter_weight_mapper.get(key)

        return (
            round(value, 2)
            if kwargs.get("currency") is None
            else round(convert_value_to_currency(value, kwargs["currency"]), 2)
        )
