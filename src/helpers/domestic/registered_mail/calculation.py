from src.helpers.currency import get_currency_schema, convert_value_to_currency
from src.helpers.domestic.registered_mail.mapper import (
    registered_mail_weight_mapper,
    registered_mail_additional_sum_mapper,
)
from src.helpers.validator import Validator


def registered_mail_validator(*args, **kwargs):
    data = Validator.remove_empty_args(kwargs)
    max_weight = list(registered_mail_weight_mapper.keys())[-1]
    additional_sum_params = list(registered_mail_additional_sum_mapper.keys())

    if kwargs.get("additional") is not None:
        data["additional"] = [
            param.strip().lower() for param in kwargs["additional"].split(",")
        ]

    schema = {
        "weight": {
            "required": True,
            "nullable": False,
            "numeric": True,
            "coerce": "numeric",
            "max": max_weight,
            "min": 0.1,
        },
        "additional": {
            "required": False,
            "nullable": True,
            "type": "list",
            "allowed": additional_sum_params,
        },
        "currency": get_currency_schema(),
    }
    Validator.validate(data, schema)

    return Validator.c_errors if Validator.errors else None


def calculate_registered_mail_cost(*args, **kwargs):
    weight = float(kwargs["weight"])
    value = None

    for key in registered_mail_weight_mapper:

        if weight > key or weight == 0:
            continue

        value = registered_mail_weight_mapper.get(key)
        break

    if kwargs.get("additional") is not None:
        additional = [
            param.strip().lower() for param in kwargs["additional"].split(",")
        ]
    else:
        additional = []

    for param in additional:
        sum_param = registered_mail_additional_sum_mapper.get(param)
        value += sum_param

    return (
        round(value, 2)
        if kwargs.get("currency") is None
        else round(convert_value_to_currency(value, kwargs["currency"]), 2)
    )
