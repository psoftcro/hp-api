registered_mail_weight_mapper = {
    50: 11.50,
    100: 13,
    250: 15,
    500: 18.50,
    1000: 22.50,
    2000: 29,
}
registered_mail_additional_sum_mapper = {"plus": 8, "ar": 3.1, "cod": 6, "ptr": 8}
