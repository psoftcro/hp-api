priority_letter_weight_mapper = {
    50: 6.50,
    100: 10,
    250: 12,
    500: 15,
    1000: 18.50,
    2000: 23.50,
}
