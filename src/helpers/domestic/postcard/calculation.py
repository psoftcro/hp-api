from src.helpers.currency import get_currency_schema, convert_value_to_currency
from src.helpers.domestic.postcard.mapper import postcard_type_mapper
from src.helpers.validator import Validator


def postcard_validator(*args, **kwargs):
    kwargs["postcard_type"] = (
        kwargs["postcard_type"].lower()
        if kwargs.get("postcard_type") is not None
        else None
    )
    data = Validator.remove_empty_args(kwargs)
    postcard_types = list(postcard_type_mapper.keys())
    schema = {
        "postcard_type": {
            "required": True,
            "nullable": False,
            "type": "string",
            "allowed": postcard_types,
        },
        "currency": get_currency_schema(),
    }
    Validator.validate(data, schema)

    return Validator.c_errors if Validator.errors else None


def calculate_postcard_cost(*args, **kwargs):
    postcard_type = kwargs["postcard_type"].lower()
    value = postcard_type_mapper.get(postcard_type)

    return (
        round(value, 2)
        if kwargs.get("currency") is None
        else round(convert_value_to_currency(value, kwargs["currency"]), 2)
    )
