from src.helpers.currency import get_currency_schema, convert_value_to_currency
from src.helpers.domestic.insured_mail.mapper import (
    insured_mail_weight_mapper,
    insured_mail_value_mapper,
    insured_mail_additional_sum_mapper,
)
from src.helpers.validator import Validator


def insured_mail_validator(*args, **kwargs):
    data = Validator.remove_empty_args(kwargs)
    max_weight = list(insured_mail_weight_mapper.keys())[-1]
    denoted_value_list = list(insured_mail_value_mapper.keys())
    lowest_value, highest_value = denoted_value_list[0], denoted_value_list[-1]
    additional_sum_params = list(insured_mail_additional_sum_mapper.keys())

    if kwargs.get("additional") is not None:
        data["additional"] = [
            param.strip().lower() for param in kwargs["additional"].split(",")
        ]

    schema = {
        "weight": {
            "required": True,
            "nullable": False,
            "numeric": True,
            "coerce": "numeric",
            "max": max_weight,
            "min": 0.1,
        },
        "denoted_value": {
            "required": True,
            "nullable": False,
            "numeric": True,
            "coerce": "numeric",
            "min": lowest_value,
            "max": highest_value,
        },
        "additional": {
            "required": False,
            "nullable": True,
            "type": "list",
            "allowed": additional_sum_params,
        },
        "currency": get_currency_schema(),
    }
    Validator.validate(data, schema)

    return Validator.c_errors if Validator.errors else None


def calculate_insured_mail_cost(*args, **kwargs):
    weight = float(kwargs["weight"])
    denoted_value = float(kwargs["denoted_value"])
    value = None

    for key in insured_mail_weight_mapper:

        if weight > key or weight == 0:
            continue

        value = insured_mail_weight_mapper.get(key)
        break

    for key in insured_mail_value_mapper:

        if denoted_value > key or denoted_value == 0:
            continue

        value += insured_mail_value_mapper.get(key)
        break

    if kwargs.get("additional") is not None:
        additional = [
            param.strip().lower() for param in kwargs["additional"].split(",")
        ]
    else:
        additional = []

    for param in additional:
        sum_param = insured_mail_additional_sum_mapper.get(param)
        value += sum_param

    return (
        round(value, 2)
        if kwargs.get("currency") is None
        else round(convert_value_to_currency(value, kwargs["currency"]), 2)
    )
