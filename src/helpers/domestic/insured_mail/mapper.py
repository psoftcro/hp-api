insured_mail_weight_mapper = {
    50: 17.50,
    100: 19,
    250: 21,
    500: 24,
    1000: 28,
    2000: 34.50,
}
insured_mail_value_mapper = {
    7.20: 7.20,
    200: 7.20,
    1000: 15,
    10000: 60,
    50000: 120,
    99999: 300,
}
insured_mail_additional_sum_mapper = {"plus": 8, "ar": 3.1, "cod": 6, "ptr": 8}
