from src.helpers.currency import get_currency_schema, convert_value_to_currency
from src.helpers.domestic.parcel_24.mapper import (
    parcel_24_weight_mapper,
    parcel_24_city_mapper,
    parcel_24_additional_sum_mapper,
    parcel_24_additional_mul_mapper,
    parcel_24_min_denoted_value,
    parcel_24_max_denoted_value,
    parcel_24_min_length,
    parcel_24_max_length,
    parcel_24_min_width,
    parcel_24_max_width,
    parcel_24_min_height,
    parcel_24_max_height,
    parcel_24_max_weight_home_address,
    parcel_24_max_weight_post_office,
    parcel_24_max_dimensions,
    parcel_24_money_wire_mapper,
    parcel_24_min_ransom,
    parcel_24_max_ransom,
)
from src.helpers.validator import Validator


def parcel_24_validator(*args, **kwargs):
    data = Validator.remove_empty_args(kwargs)
    max_weight = list(parcel_24_weight_mapper.keys())[-1]
    additional_sum_params = list(parcel_24_additional_sum_mapper.keys())
    is_cod = False
    additional_mul_params = list(parcel_24_additional_mul_mapper.keys())
    additional_parcel_24_params = additional_sum_params + additional_mul_params
    delivery_locations = list(
        parcel_24_weight_mapper.get(next(iter(parcel_24_weight_mapper)))
        .get("delivery_location")
        .keys()
    )
    delivery_times = list(
        parcel_24_weight_mapper.get(next(iter(parcel_24_weight_mapper)))
        .get("delivery_time")
        .keys()
    )

    if kwargs.get("additional") is not None:
        if "cod" in kwargs["additional"]:
            is_cod = True
        data["additional"] = [
            param.strip().lower() for param in kwargs["additional"].split(",")
        ]

    schema = {
        "weight": {
            "numeric": True,
            "nullable": False,
            "coerce": "numeric",
            "excludes": ["length", "width", "height", "dimension_sum"],
            "required": True,
            "max": max_weight,
            "min": 0.1,
        },
        "delivery_location": {
            "required": True,
            "nullable": False,
            "type": "string",
            "allowed": delivery_locations,
        },
        "delivery_time": {
            "required": True,
            "nullable": False,
            "type": "string",
            "allowed": delivery_times,
        },
        "deliver_from": {
            "required": False,
            "nullable": False,
            "type": "string",
            "allowed": list(parcel_24_city_mapper.keys()),
        },
        "deliver_to": {
            "required": False,
            "nullable": False,
            "type": "string",
            "allowed": list(parcel_24_city_mapper.keys()),
        },
        "denoted_value": {
            "required": True,
            "nullable": False,
            "numeric": True,
            "coerce": "numeric",
            "min": parcel_24_min_denoted_value,
            "max": parcel_24_max_denoted_value,
        },
        "additional": {
            "required": False,
            "nullable": True,
            "check_domestic_parcel_24_cod_requirements": is_cod,
            "type": "list",
            "allowed": additional_parcel_24_params,
        },
        "length": {
            "numeric": True,
            "coerce": "numeric",
            "nullable": False,
            "dependencies": ["width", "height"],
            "min": parcel_24_min_length,
            "max": parcel_24_max_length,
            "excludes": ["weight"],
            "required": True,
        },
        "width": {
            "numeric": True,
            "coerce": "numeric",
            "nullable": False,
            "dependencies": ["length", "height"],
            "min": parcel_24_min_width,
            "max": parcel_24_max_width,
            "excludes": ["weight"],
            "required": True,
        },
        "height": {
            "numeric": True,
            "coerce": "numeric",
            "nullable": False,
            "check_domestic_parcel_24_dimension_limit": parcel_24_max_dimensions,
            "dependencies": ["length", "width"],
            "min": parcel_24_min_height,
            "max": parcel_24_max_height,
            "excludes": ["weight"],
            "required": True,
        },
        "ransom": {
            "numeric": True,
            "coerce": "numeric",
            "nullable": False,
            "check_domestic_parcel_24_additional": is_cod,
            "min": parcel_24_min_ransom,
            "max": parcel_24_max_ransom,
            "required": False,
        },
        "money_wire": {
            "required": False,
            "nullable": False,
            "check_domestic_parcel_24_additional": is_cod,
            "type": "string",
            "allowed": parcel_24_money_wire_mapper,
        },
        "currency": get_currency_schema(),
    }
    Validator.validate(data, schema)

    return Validator.c_errors if Validator.errors else None


def calculate_parcel_24_cost(*args, **kwargs):
    denoted_value = float(kwargs["denoted_value"])
    if kwargs.get("ransom") is not None:
        ransom = float(kwargs["ransom"])
    else:
        ransom = None
    home_delivery = False
    value = None
    volume = None
    over = 0

    if kwargs.get("weight") is not None:
        weight = float(kwargs["weight"])
    else:
        weight = None

    if not weight:
        if (
            kwargs.get("length") is not None
            and kwargs.get("width") is not None
            and kwargs.get("height") is not None
        ):
            length = float(kwargs["length"])
            width = float(kwargs["width"])
            height = float(kwargs["height"])
            volume = float(length * width * height / 5000)

            if kwargs["delivery_location"] == "home_address":
                home_delivery = True
                if volume > parcel_24_max_weight_home_address:
                    weight = parcel_24_max_weight_home_address
                else:
                    weight = volume
            if kwargs["delivery_location"] == "post_office":
                if volume > parcel_24_max_weight_post_office:
                    weight = parcel_24_max_weight_post_office
                else:
                    weight = volume

    for key in parcel_24_weight_mapper:

        if weight > key or weight == 0:
            continue

        value = (
            parcel_24_weight_mapper.get(key)
            .get("delivery_location")
            .get(kwargs["delivery_location"])
        )
        break

    if not value:
        return None

    if kwargs.get("deliver_from") is None or kwargs.get("deliver_to") is None:
        value += 12.5

    if kwargs.get("additional") is not None:
        additional = [
            param.strip().lower() for param in kwargs["additional"].split(",")
        ]
    else:
        additional = []

    for param, num in parcel_24_additional_sum_mapper.items():

        if param not in additional:
            continue

        value += num

    if ransom and kwargs.get("money_wire") is not None:
        if kwargs["money_wire"] == parcel_24_money_wire_mapper[0]:
            if ransom <= 273.22:
                value += 5
            elif 273.22 < ransom <= 4731.59:
                value = value + (ransom * 0.0183)
            else:
                value += 80
        else:
            if ransom <= 240:
                value += 9
            else:
                value = value + (ransom * 0.0375)

    for param, num in parcel_24_additional_mul_mapper.items():

        if param not in additional:
            continue

        value = value + (value * num)

    if volume:

        if home_delivery and volume > parcel_24_max_weight_home_address:
            over = volume - parcel_24_max_weight_home_address

        if not home_delivery and volume > parcel_24_max_weight_post_office:
            over = volume - parcel_24_max_weight_post_office

    if over > 0:
        check = over / 5
        if over % 5 > 0:
            check += 1
        value = value + (round(check) * 10)

    if 500 < denoted_value <= 2100:
        value += 5
    elif denoted_value > 2100:
        top_up = denoted_value * 0.25 / 100
        value = value + top_up

    return (
        round(value, 2)
        if kwargs.get("currency") is None
        else round(convert_value_to_currency(value, kwargs["currency"]), 2)
    )
