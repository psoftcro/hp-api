"""
####  Parameter Description  ####
ar: Advice of delivery
cod: Cash-on-delivery
nsf: Non-standard format
ptr: Personally to the recipient
rcd: Return of certified document
todos: Taking over/delivery on Saturday
9am: Delivery by 9am
11am: Delivery by 11am
"""

parcel_24_weight_mapper = {
    1000: {
        "delivery_location": {"home_address": 39, "post_office": 39 * 0.9},
        "delivery_time": {
            "one_working_day": None,
            "two_working_days": None,
            "three_working_days": None,
        },
    },
    2000: {
        "delivery_location": {"home_address": 44, "post_office": 44 * 0.9},
        "delivery_time": {
            "one_working_day": None,
            "two_working_days": None,
            "three_working_days": None,
        },
    },
    5000: {
        "delivery_location": {"home_address": 49, "post_office": 49 * 0.9},
        "delivery_time": {
            "one_working_day": None,
            "two_working_days": None,
            "three_working_days": None,
        },
    },
    10000: {
        "delivery_location": {"home_address": 53, "post_office": 53 * 0.9},
        "delivery_time": {
            "one_working_day": None,
            "two_working_days": None,
            "three_working_days": None,
        },
    },
    15000: {
        "delivery_location": {"home_address": 69, "post_office": 69 * 0.9},
        "delivery_time": {
            "one_working_day": None,
            "two_working_days": None,
            "three_working_days": None,
        },
    },
    20000: {
        "delivery_location": {"home_address": 83, "post_office": 83 * 0.9},
        "delivery_time": {
            "one_working_day": None,
            "two_working_days": None,
            "three_working_days": None,
        },
    },
    30000: {
        "delivery_location": {"home_address": 103, "post_office": 103 * 0.9},
        "delivery_time": {
            "one_working_day": None,
            "two_working_days": None,
            "three_working_days": None,
        },
    },
}
parcel_24_city_mapper = {
    "beli_manastir": 1,
    "bestovje": 1,
    "bibinje": 1,
    "bilaj": 1,
    "bilice": 1,
    "bjelovar": 1,
    "botinec": 1,
    "bracak": 1,
    "bregi_zabocki": 1,
    "brezovica": 1,
    "brisevo": 1,
    "buzin": 1,
    "cakovec": 1,
    "cazma": 1,
    "cemernica": 1,
    "crikvenica": 1,
    "crno": 1,
    "daruvar": 1,
    "delnice": 1,
    "djakovo": 1,
    "djurdjevac": 1,
    "donji_cehi": 1,
    "donji_miholjac": 1,
    "donji_stupnik": 1,
    "dubrava_kod_sibenika": 1,
    "dubrovnik": 1,
    "dugo_selo": 1,
    "dugopolje": 1,
    "golo_brdo": 1,
    "gornji_cehi": 1,
    "gornji_stupnik": 1,
    "gospic": 1,
    "grubisno_polje": 1,
    "hrasce_turopoljsko": 1,
    "hrvatski_leskovac": 1,
    "hum_zabocki": 1,
    "imotski": 1,
    "ivanec": 1,
    "ivanic_grad": 1,
    "jezdovec": 1,
    "kamen": 1,
    "kaniza_gospicka": 1,
    "karlovac": 1,
    "kastel_gomilica": 1,
    "kastel_kambelovac": 1,
    "kastel_luksic": 1,
    "kastel_novi": 1,
    "kastel_stafilic": 1,
    "kastel_stari": 1,
    "kastel_sucurac": 1,
    "koprivnica": 1,
    "korija": 1,
    "krapina": 1,
    "krizevci": 1,
    "krk": 1,
    "kutina": 1,
    "lucko": 1,
    "ludbreg": 1,
    "lug_zabocki": 1,
    "makarska": 1,
    "mala_mlaka": 1,
    "metkovic": 1,
    "milanovac": 1,
    "mirkovci": 1,
    "murvica": 1,
    "naselje_a_hebrang": 1,
    "nasice": 1,
    "nedelisce": 1,
    "nova_gradiska": 1,
    "novi_marof": 1,
    "novska": 1,
    "obrez_stupnicki": 1,
    "odra": 1,
    "odranski_obrez": 1,
    "ogulin": 1,
    "omis": 1,
    "opatija": 1,
    "orahovica": 1,
    "osijek": 1,
    "other": 2,
    "otocac": 1,
    "ozalj": 1,
    "pakrac": 1,
    "petrinja": 1,
    "ploce": 1,
    "podgorje_virovitica": 1,
    "policnik": 1,
    "porec": 1,
    "pozari": 1,
    "pozega": 1,
    "pula": 1,
    "rezovac": 1,
    "rijeka": 1,
    "samobor": 1,
    "senj": 1,
    "sesvete": 1,
    "sibenik": 1,
    "sinj": 1,
    "sisak": 1,
    "sisak_caprag": 1,
    "slavonski_brod": 1,
    "sljeme": 1,
    "solin": 1,
    "split": 1,
    "strmec": 1,
    "sukosan": 1,
    "sveta_nedjelja": 1,
    "sveti_ivan_zelina": 1,
    "trogir": 1,
    "umag": 1,
    "valpovo": 1,
    "varazdin": 1,
    "velika_gorica": 1,
    "velika_mlaka": 1,
    "veliko_polje": 1,
    "vinkovci": 1,
    "virovitica": 1,
    "vrbovec": 1,
    "vrgorac": 1,
    "vukovar": 1,
    "zabok": 1,
    "zadar": 1,
    "zagreb": 1,
    "zapresic": 1,
    "zbandaj": 1,
    "zupanja": 1,
    "zutnica": 1,
}
parcel_24_additional_sum_mapper = {
    "ar": 3.88,
    "cod": 7.5,
    "nsf": 93.75,
    "ptr": 10,
    "rcd": 15,
    "9am": 32.5,
    "11am": 17.5,
}
parcel_24_additional_mul_mapper = {"todos": 0.5}
parcel_24_money_wire_mapper = ["hub1", "postal"]
parcel_24_min_denoted_value = 3
parcel_24_max_denoted_value = 100000

parcel_24_min_length = 9
parcel_24_max_length = 200
parcel_24_min_height = 9
parcel_24_max_height = 200
parcel_24_min_ransom = 0.1
parcel_24_max_ransom = 25000
parcel_24_min_width = 9
parcel_24_max_width = 200
parcel_24_max_weight_post_office = 20
parcel_24_max_weight_home_address = 30
parcel_24_max_dimensions = 350
