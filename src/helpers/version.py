import datetime
import difflib
import os

import requests
from slack import WebClient
from slack.errors import SlackApiError

from src.config import Config

FILE_NAME = "hp_script.js"
FILE_DIR = "static"


def _get_script():
    resp = requests.get("https://www.posta.hr/js/icp_modal.js")
    return resp.text


def _read_file(filename):
    file = open(filename, "r")
    content = file.read()
    file.close()
    return content


def _check_file(filename):
    if not os.path.isfile(filename):
        return False
    return True


def _store_file(filename, content):
    file = open(filename, "w")
    file.write(content)
    file.close()


def check_updates():
    file_path = f"{FILE_DIR}/{FILE_NAME}"
    script = _get_script()

    if not _check_file(file_path):
        _store_file(file_path, script)
        return

    local_script = _read_file(file_path)
    html_diff = difflib.HtmlDiff()

    if script == local_script:
        return

    html = html_diff.make_file(
        context=True, fromlines=local_script.split("\n"), tolines=script.split("\n")
    )
    diff_file_name = f"{str(datetime.date.today())}.html"
    diff_path = f"{FILE_DIR}/{diff_file_name}"
    _store_file(diff_path, html)
    _store_file(file_path, script)
    try:
        client = WebClient(token=Config.SLACK_TOKEN)
        response = client.files_upload(channels="hp-updates", file=diff_path)
        assert response["file"]
        os.remove(diff_path)
    except SlackApiError as e:
        assert e.response["ok"] is False
        assert e.response["error"]
        print(f"Got an error: {e.response['error']}")
