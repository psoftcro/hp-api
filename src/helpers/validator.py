from cerberus import Validator


class Validation(Validator):
    def __init__(self, *args, **kwargs):
        super(Validation, self).__init__(*args, **kwargs)
        self.data = None

    @property
    def c_errors(self):
        response = {}

        for field, error_list in self.errors.items():
            error_list = list(set(error_list))
            response[field] = [e.capitalize() for e in error_list]

        return {"errors": response}

    @staticmethod
    def remove_empty_args(data):
        new_data = {}

        for key, value in data.items():

            if value is None or value == "":
                continue

            new_data[key] = value

        return new_data

    @staticmethod
    def _normalize_coerce_numeric(value):
        if value is None:
            return None

        return float(value)

    def _validate_check_domestic_parcel_24_dimension_limit(self, limit, field, value):
        """
        Test that domestic parcel_24 dimension sum does not breach given limit.

        The rule's arguments are validated against this schema:
        {'type': 'number'}
        """
        length = self.document.get("length")
        width = self.document.get("width")
        height = self.document.get("height")

        if not length or not width or not height:
            return

        try:
            float(length)
            float(width)
            float(height)

        except ValueError:
            return

        dimension_value = length + width + height

        if dimension_value > limit:
            self._error(
                "dimensions",
                f"sum of dimensions ({dimension_value}) is larger than {limit}",
            )

    def _validate_check_domestic_parcel_24_additional(self, check, field, value):
        """
        Test if domestic parcel_24 field is valid for given additional parameter.

        The rule's arguments are validated against this schema:
        {'type': 'boolean'}
        """

        if check:
            return

        additional = self.document.get("additional")
        if not check:
            self._error(
                f"{field}",
                f"must not be present when 'additional' is '{additional}'",
            )

    def _validate_check_domestic_parcel_24_cod_requirements(self, check, field, value):
        """
        Test if domestic parcel-24 cod additional parameter is used with required fields.

        The rule's arguments are validated against this schema:
        {'type': 'boolean'}
        """

        ransom = self.document.get("ransom")
        money_wire = self.document.get("money_wire")

        if check:
            if not ransom or not money_wire:
                self._error(
                    "cod",
                    f"must not be present when 'ransom' is '{ransom}', and 'money_wire' is {money_wire}",
                )

    def _validate_check_int_parcel_type_additionals(self, check, field, value):
        """
        Test if int parcel additional and parcel type parameters are allowed for specific country

        The rule's arguments are validated against this schema:
        {'type': 'boolean'}
        """
        if not check:
            return

        parcel_country_mapper = self.data
        country = self.document.get("country")
        additional = self.document.get("additional")
        parcel_type = self.document.get("parcel_type")

        if not parcel_country_mapper.get(country):
            return

        if not parcel_type:
            return

        if (
            parcel_type
            not in parcel_country_mapper.get(country).get("parcel_type").keys()
        ):
            self._error(
                "parcel_type",
                f"must not be '{parcel_type}' when 'country' is '{country}'",
            )
            return

        if not additional:
            return

        additional_keys = (
            parcel_country_mapper.get(country)
            .get("parcel_type")
            .get(parcel_type)
            .get("additional")
        )

        unallowed_params = []

        for param in additional:
            if param in additional_keys:
                continue

            unallowed_params.append(param)

        if unallowed_params:
            self._error(
                "additional",
                f"unallowed values {unallowed_params} when 'parcel_type' is '{parcel_type}'",
            )

    def _validate_check_int_priority_insured_mail_additionals(
        self, check, field, value
    ):
        """
        Test if int priority insured mail additional parameters are allowed for specific country

        The rule's arguments are validated against this schema:
        {'type': 'boolean'}
        """
        if not check:
            return

        priority_insured_mail_country_mapper = self.data
        country = self.document.get("country")
        additional = self.document.get("additional")

        if not priority_insured_mail_country_mapper.get(country):
            return

        if not additional:
            return

        additional_keys = priority_insured_mail_country_mapper.get(country).get(
            "additional"
        )
        unallowed_params = []

        for param in additional:
            if param in additional_keys:
                continue

            unallowed_params.append(param)

        if unallowed_params:
            self._error(
                "additional",
                f"unallowed values {unallowed_params}",
            )

    def _validate_check_int_parcel_insured_value(self, check, field, value):
        """
        Test if int parcel insured value is allowed for specific country and parcel type

        The rule's arguments are validated against this schema:
        {'type': 'boolean'}
        """
        if not check:
            return

        parcel_country_mapper = self.data
        country = self.document.get("country")
        parcel_type = self.document.get("parcel_type")

        if not parcel_country_mapper.get(country):
            return

        if (
            not not parcel_country_mapper.get(country)
            .get("parcel_type")
            .get(parcel_type)
        ):
            return

        if (
            not parcel_country_mapper.get(country)
            .get("parcel_type")
            .get(parcel_type)
            .get("allowed_with_insured_value")
        ):
            self._error(
                "insured_value",
                f"must not be present when 'country' is '{country}' and 'parcel_type' is '{parcel_type}'",
            )

    def _validate_check_int_priority_insured_mail_denoted_value(
        self, check, field, value
    ):
        """
        Test if int priority insured mail denoted value is allowed for specific country

        The rule's arguments are validated against this schema:
        {'type': 'boolean'}
        """
        if not check:
            return

        priority_insured_mail_country_mapper = self.data
        country = self.document.get("country")
        denoted_value = self.document.get("denoted_value")

        if not priority_insured_mail_country_mapper.get(country):
            return

        if not denoted_value or denoted_value == 0:
            return

        try:
            denoted_value = float(denoted_value)

        except ValueError:
            return

        # Commented out due to HP denoted_value max error
        # base_value = priority_insured_mail_country_mapper.get(country).get(
        #    "denoted_value_base")

        # if not base_value:
        #    return

        # calculated_value = denoted_value * 0.1125
        max_denoted_value = priority_insured_mail_country_mapper.get(country).get(
            "max_denoted_value"
        )

        # Changed condition due to HP denoted_value max error
        if denoted_value > max_denoted_value:
            self._error(
                "denoted_value",
                f"max 'denoted_value' for '{country}' is '{max_denoted_value}'",
            )

    def _validate_check_int_hp_express_max_weight(self, check, field, value):
        """
        Test if int hp express weight is allowed for specific country

        The rule's arguments are validated against this schema:
        {'type': 'boolean'}
        """
        if not check:
            return

        hp_express_country_mapper = self.data
        country = self.document.get("country")
        weight = self.document.get("weight")

        if not hp_express_country_mapper.get(country):
            return

        if not weight or weight == 0:
            return

        try:
            weight = float(weight)

        except ValueError:
            return

        max_weight = hp_express_country_mapper.get(country).get("max_weight")

        if weight > max_weight:
            self._error(
                "weight",
                f"max 'weight' for '{country}' is '{max_weight}'",
            )

    def _validate_numeric(self, check, field, value):
        """
        Test that field has numeric value (string, int, float).

        The rule's arguments are validated against this schema:
        {'type': 'boolean'}
        """
        if not value:
            return None

        try:
            float(value)
            if not check:
                self._error(field, "must not be a numeric value")

        except ValueError:
            if check:
                self._error(field, "must be a numeric value")


Validator = Validation()
