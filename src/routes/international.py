from typing import Optional

from fastapi import APIRouter, Request, Query

from src.helpers.docs.params import (
    Currency,
    Country,
    DeliveryLocation,
    ParcelType,
)

from src.helpers.international.hp_express import (
    calculate_hp_express_cost,
    hp_express_validator,
)
from src.helpers.international.imprime import (
    calculate_imprime_cost,
    imprime_validator,
)
from src.helpers.international.letter import calculate_letter_cost, letter_validator
from src.helpers.international.m_bag import (
    calculate_m_bag_cost,
    m_bag_validator,
)
from src.helpers.international.parcel import calculate_parcel_cost, parcel_validator
from src.helpers.international.postcard import (
    calculate_postcard_cost,
    postcard_validator,
)
from src.helpers.international.priority_insured_mail import (
    calculate_priority_insured_mail_cost,
    priority_insured_mail_validator,
)
from src.helpers.international.priority_letter import (
    calculate_priority_letter_cost,
    priority_letter_validator,
)
from src.helpers.international.priority_registered_mail import (
    calculate_priority_registered_mail_cost,
    priority_registered_mail_validator,
)
from src.helpers.international.registered_m_bag import (
    calculate_registered_m_bag_cost,
    registered_m_bag_validator,
)
from src.modules.responses import get_response

router = APIRouter()


@router.get("/international/hp-express", tags=["International"])
async def calculate_hp_express(
    request: Request,
    country: Optional[Country] = Query(..., description="Destination country"),
    weight: float = Query(..., description="Weight in grams (0.1-2000)"),
    delivery_location: DeliveryLocation = Query(..., description="Delivery location"),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = hp_express_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_hp_express_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)


@router.get("/international/imprime", tags=["International"])
async def calculate_imprime(
    request: Request,
    country: Optional[Country] = Query(..., description="Destination country"),
    weight: float = Query(..., description="Weight in grams (0.1-50000)"),
    additional: Optional[str] = Query(
        default=None, description="Additional parameters (comma separated)"
    ),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = imprime_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_imprime_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)


@router.get("/international/letter", tags=["International"])
async def calculate_letter(
    request: Request,
    country: Optional[Country] = Query(..., description="Destination country"),
    weight: float = Query(..., description="Weight in grams (0.1-2000)"),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = letter_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_letter_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)


@router.get("/international/m-bag", tags=["International"])
async def calculate_m_bag(
    request: Request,
    country: Optional[Country] = Query(..., description="Destination country"),
    weight: float = Query(..., description="Weight in kilograms"),
    additional: Optional[str] = Query(
        default=None, description="Additional parameters (comma separated)"
    ),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = m_bag_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_m_bag_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)


@router.get("/international/parcel", tags=["International"])
async def calculate_parcel(
    request: Request,
    country: Optional[Country] = Query(..., description="Destination country"),
    weight: float = Query(..., description="Weight in kilograms"),
    parcel_type: Optional[ParcelType] = Query(..., description="Parcel type"),
    insured_value: float = Query(default=None, description="Insured value"),
    additional: Optional[str] = Query(
        default=None, description="Additional parameters (comma separated)"
    ),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = parcel_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_parcel_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)


@router.get("/international/postcard", tags=["International"])
async def calculate_postcard(
    request: Request,
    country: Optional[Country] = Query(..., description="Destination country"),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = postcard_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_postcard_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)


@router.get("/international/priority-insured-mail", tags=["International"])
async def calculate_priority_insured_mail(
    request: Request,
    country: Optional[Country] = Query(..., description="Destination country"),
    weight: float = Query(..., description="Weight in grams"),
    denoted_value: float = Query(..., description="Denoted value"),
    additional: Optional[str] = Query(
        default=None, description="Additional parameters (comma separated)"
    ),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = priority_insured_mail_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_priority_insured_mail_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)


@router.get("/international/priority-letter", tags=["International"])
async def calculate_priority_letter(
    request: Request,
    country: Optional[Country] = Query(..., description="Destination country"),
    weight: float = Query(..., description="Weight in grams"),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = priority_letter_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_priority_letter_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)


@router.get("/international/priority-registered-mail", tags=["International"])
async def calculate_priority_registered_mail(
    request: Request,
    country: Optional[Country] = Query(..., description="Destination country"),
    weight: float = Query(..., description="Weight in grams"),
    additional: Optional[str] = Query(
        default=None, description="Additional parameters (comma separated)"
    ),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = priority_registered_mail_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_priority_registered_mail_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)


@router.get("/international/registered-m-bag", tags=["International"])
async def calculate_registered_m_bag(
    request: Request,
    country: Optional[Country] = Query(..., description="Destination country"),
    weight: float = Query(..., description="Weight in kilograms"),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = registered_m_bag_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_registered_m_bag_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)
