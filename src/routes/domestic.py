from typing import Optional

from fastapi import APIRouter, Query, Request

from src.helpers.docs.params import (
    City,
    Currency,
    DeliveryLocation,
    DeliveryTime,
    MoneyWire,
    PostcardType,
)
from src.helpers.domestic.administrative_mail import (
    calculate_administrative_mail_cost,
    administrative_mail_validator,
)
from src.helpers.domestic.imprime import calculate_imprime_cost, imprime_validator
from src.helpers.domestic.insured_mail import (
    calculate_insured_mail_cost,
    insured_mail_validator,
)
from src.helpers.domestic.letter import calculate_letter_cost, letter_validator
from src.helpers.domestic.parcel import calculate_parcel_cost, parcel_validator
from src.helpers.domestic.parcel_24 import (
    calculate_parcel_24_cost,
    parcel_24_validator,
)
from src.helpers.domestic.postcard import calculate_postcard_cost, postcard_validator
from src.helpers.domestic.priority_letter import (
    calculate_priority_letter_cost,
    priority_letter_validator,
)
from src.helpers.domestic.registered_mail import (
    calculate_registered_mail_cost,
    registered_mail_validator,
)
from src.modules.responses import get_response

router = APIRouter()


@router.get("/domestic/administrative-mail", tags=["Domestic"])
async def calculate_administrative_mail(
    request: Request,
    weight: float = Query(..., description="Weight in grams (0.1-2000)"),
    additional: Optional[str] = Query(
        default=None, description="Additional parameters (comma separated)"
    ),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = administrative_mail_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_administrative_mail_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)


@router.get("/domestic/imprime", tags=["Domestic"])
async def calculate_imprime(
    request: Request,
    weight: float = Query(..., description="Weight in grams (0.1-2000)"),
    additional: Optional[str] = Query(
        default=None, description="Additional parameters (comma separated)"
    ),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = imprime_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_imprime_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)


@router.get("/domestic/insured-mail", tags=["Domestic"])
async def calculate_insured_mail(
    request: Request,
    weight: float = Query(..., description="Weight in grams (0.1-2000)"),
    denoted_value: float = Query(..., description="Denoted value (7.2-99999 HRK)"),
    additional: Optional[str] = Query(
        default=None, description="Additional parameters (comma separated)"
    ),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = insured_mail_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_insured_mail_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)


@router.get("/domestic/letter", tags=["Domestic"])
async def calculate_letter(
    request: Request,
    weight: float = Query(..., description="Weight in grams (0.1-2000)"),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = letter_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_letter_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)


@router.get("/domestic/parcel", tags=["Domestic"])
async def calculate_parcel(
    request: Request,
    weight: float = Query(..., description="Weight in grams (0.1-20000)"),
    denoted_value: float = Query(..., description="Denoted value (3-100000 HRK)"),
    delivery_location: DeliveryLocation = Query(..., description="Delivery location"),
    additional: Optional[str] = Query(
        default=None, description="Additional parameters (comma separated)"
    ),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = parcel_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_parcel_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)


@router.get("/domestic/parcel-24", tags=["Domestic"])
async def calculate_parcel_24(
    request: Request,
    weight: Optional[float] = Query(
        default=None, description="Weight in grams (0.1-50000)"
    ),
    denoted_value: float = Query(..., description="Denoted value (3-100000 HRK)"),
    delivery_location: DeliveryLocation = Query(..., description="Delivery location"),
    delivery_time: DeliveryTime = Query(..., description="Delivery location"),
    deliver_from: City = Query(..., description="Deliver from location"),
    deliver_to: City = Query(..., description="Deliver to location"),
    length: Optional[float] = Query(
        default=None, description="Length of the package in cm (9-200)"
    ),
    width: Optional[float] = Query(
        default=None, description="Width of the package in cm (9-200)"
    ),
    height: Optional[float] = Query(
        default=None, description="Height of the package in cm (9-200)"
    ),
    ransom: Optional[float] = Query(
        default=None, description="Ransom value (0.1-25000 HRK)"
    ),
    money_wire: Optional[MoneyWire] = Query(default=None, description="Money wire"),
    additional: Optional[str] = Query(
        default=None, description="Additional parameters (comma separated)"
    ),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = parcel_24_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_parcel_24_cost(**data)

    params = {"value": value}
    return get_response(200, params=params)


@router.get("/domestic/postcard", tags=["Domestic"])
async def calculate_postcard(
    request: Request,
    postcard_type: PostcardType = Query(..., description="Type of the postcard"),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = postcard_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_postcard_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)


@router.get("/domestic/priority-letter", tags=["Domestic"])
async def calculate_priority_letter(
    request: Request,
    weight: float = Query(..., description="Weight in grams (0.1-2000)"),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = priority_letter_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_priority_letter_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)


@router.get("/domestic/registered-mail", tags=["Domestic"])
async def calculate_registered_mail(
    request: Request,
    weight: float = Query(..., description="Weight in grams (0.1-2000)"),
    additional: Optional[str] = Query(
        default=None, description="Additional parameters (comma separated)"
    ),
    currency: Optional[Currency] = Query(
        default=None, description="Currency to be converted to (HRK by default)"
    ),
):
    data = request.query_params
    errors = registered_mail_validator(**data)

    if errors:
        return get_response(422, params=errors)

    value = calculate_registered_mail_cost(**data)
    params = {"value": value}

    return get_response(200, params=params)
