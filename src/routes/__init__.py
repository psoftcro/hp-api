from src.routes.domestic import router as domestic
from src.routes.international import router as international
from src.routes.tracking import router as tracking

routes = [domestic, international, tracking]
