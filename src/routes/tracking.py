from fastapi import APIRouter, Query, Request
from hp_tracker import HPTracker, TrackingStatusError

from src.modules.responses import get_response

router = APIRouter()


@router.get(
    "/tracking",
    tags=["Tracking"],
    responses={
        200: {
            "description": "Success",
            "content": {
                "application/json": {
                    "example": {
                        "23.12.2020 02:10:00": "Some parcel tracking event",
                        "23.12.2020 03:16:00": "Other parcel tracking event",
                    }
                }
            },
        },
        422: {
            "description": "Validation Error",
            "content": {
                "application/json": {
                    "example": {
                        "code": 422,
                        "message": "Validation Error",
                        "description": "Tracking status unavailable for tracking number: 345345",
                    }
                }
            },
        },
    },
)
async def track_parcel(
    request: Request,
    tracking_no: str = Query(
        ..., description="Tracking number to get track status for"
    ),
):
    data = request.query_params
    if data.get("tracking_no", None) is None:
        return get_response(422)
    hp = HPTracker()
    try:
        status = hp.track(data["tracking_no"]) or {}
        return get_response(200, params=status)
    except TrackingStatusError as ex:
        return get_response(422, description=ex.message)
