import os


class Config:
    # app
    APP_TITLE = os.getenv("APP_TITLE", "HP Api")
    DEBUG = os.getenv("DEBUG", "1") == "1"
    TESTING = os.getenv("TESTING", "0") == "1"
    ENV = os.getenv("ENV", "development")
    HOST = os.getenv("HOST", "0.0.0.0")
    PORT = int(os.getenv("PORT", "5000"))
    SECRET_KEY = os.getenv("SECRET_KEY", "12345678")
    SLACK_TOKEN = os.getenv("SLACK_TOKEN", "12345678")
    PACKAGE_VERSION = os.getenv("PACKAGE_VERSION", "dev")

    # gunicorn
    GUNICORN_BACKLOG = int(os.getenv("GUNICORN_BACKLOG", "5000"))
    GUNICORN_WORKER_CONNECTIONS = int(os.getenv("GUNICORN_WORKER_CONNECTIONS", "500"))
    GUNICORN_WORKER_MAX_REQUESTS = int(os.getenv("GUNICORN_WORKER_MAX_REQUESTS", "100"))
    WEB_CONCURRENCY = int(os.getenv("WEB_CONCURRENCY", "1"))

    # redis
    REDIS_HOST = os.getenv("REDIS_HOST", "redis")
    REDIS_PORT = int(os.getenv("REDIS_PORT", "6379"))
    REDIS_DB = int(os.getenv("REDIS_DB", "0"))
    REDIS_URL = f"redis://{REDIS_HOST}:{REDIS_PORT}/{REDIS_DB}"

    # docs
    DOCS_VERSION = os.getenv("DOCS_VERSION", "0.5")


config = Config()
