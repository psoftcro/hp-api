from fastapi import FastAPI
from fastapi.openapi.docs import get_redoc_html, get_swagger_ui_html
from fastapi.openapi.utils import get_openapi
from fastapi.staticfiles import StaticFiles

from src.config import config
from src.helpers.docs.constants import responses, tags_metadata
from src.routes import routes


def configure_app(app):
    _configure_routes(app)
    _configure_openapi(app)


def _configure_routes(app):
    for router in routes:
        app.include_router(router, responses=responses)


def _configure_openapi(app):
    def _custom_openapi():
        if app.openapi_schema:
            return app.openapi_schema
        openapi_schema = get_openapi(
            title=config.APP_TITLE + " Documentation",
            version=config.DOCS_VERSION,
            routes=app.routes,
            tags=tags_metadata,
        )
        app.openapi_schema = openapi_schema
        return app.openapi_schema

    app.openapi = _custom_openapi


application = FastAPI(
    docs_url=None,
    redoc_url=None,
    title=config.APP_TITLE,
)
application.mount("/static", StaticFiles(directory="static"), name="static")


@application.get("/", include_in_schema=False)
async def swagger_ui_html():
    return get_swagger_ui_html(
        openapi_url=application.openapi_url,
        title=application.title + " - Docs",
        oauth2_redirect_url=application.swagger_ui_oauth2_redirect_url,
        swagger_js_url="/static/swagger-ui-bundle.js",
        swagger_css_url="/static/swagger-ui.css",
    )


@application.get("/redoc", include_in_schema=False)
async def redoc_html():
    return get_redoc_html(
        openapi_url=application.openapi_url,
        title=application.title + " - Docs",
        redoc_js_url="/static/redoc.standalone.js",
    )


configure_app(application)
