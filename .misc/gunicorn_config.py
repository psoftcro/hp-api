"""Link to docs: http://docs.gunicorn.org/en/stable/settings.html"""
import os


timeout = 90
backlog = int(os.getenv("GUNICORN_BACKLOG", "200"))
worker_connections = int(os.getenv("GUNICORN_WORKER_CONNECTIONS", "4"))
max_requests = int(os.getenv("GUNICORN_WORKER_MAX_REQUESTS", "10000"))
workers = int(os.getenv("WEB_CONCURRENCY", "1"))
worker_class = "uvicorn.workers.UvicornWorker"
proc_name = "hp-api"

if os.getenv("ENV") == "production":
    preload_app = True
    reload = False
else:
    preload_app = False
    reload = True

bind = f":{int(os.getenv('PORT', '5000'))}"
accesslog = "-"  # send access log to stdout
logger_class = "src.helpers.logs.HPApiGunicornLogger"
